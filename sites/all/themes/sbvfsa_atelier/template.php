<?php

function sbvmainurl(){
	return "http://www.sbv-fsa.ch/";
}



/**
 * Add body classes if certain regions have content.
 */
function sbvfsa_preprocess_html(&$variables) {

  // Homepage Language Fix
 if(selfURL() == "http://www.sbv-fsa.ch/fr"){
    header("Location: http://www.sbv-fsa.ch/fr/homefr");  
  }
  if(selfURL() == "http://wwwtest.sbv-fsa.ch/fr"){
    header("Location: http://wwwtest.sbv-fsa.ch/fr/homefr");  
  }

	// monkee.ch
	global $language;
	$_SESSION["language"] = $language->language;

  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }



  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }

  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function sbvfsa_process_html(&$variables) {
	
	
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}




/**
 * Override or insert variables into the page template.
 */
function sbvfsa_process_page(&$variables) {

	// Monkee.ch
	

	
	// Fix redirect to source-domain
	global $_domain;
	global $_node;
	global $language;

	
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function sbvfsa_preprocess_maintenance_page(&$variables) {
  if (!$variables['db_is_active']) {
    unset($variables['site_name']);
  }
  drupal_add_css(drupal_get_path('theme', 'sbvfsa') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function sbvfsa_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function sbvfsa_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}

/**
 * Override or insert variables into the block template.
 */
function sbvfsa_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function sbvfsa_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function sbvfsa_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' . $output . '</div>';

  return $output;
}


// Hide untranslated link...
function sbvfsa_links__locale_block($variables) {
  foreach($variables['links'] as $key => $value) {
    if ($value['attributes']['class']=='locale-untranslated') {
      unset($variables['links'][$key]);
    }
  }
  return theme('links', $variables);
}


// Special Pre BreadCrumbs

function beratungPreCrumb($lang){
	
	if($lang == 'fr'){
		echo '<a href="'.sbvmainurl().'fr/homefr">FSA</a> » <a href="'.sbvmainurl().'fr/node/114">Services de consultation</a>';
	} else {
		echo '<a href="'.sbvmainurl().'de">SBV</a> » <a href="'.sbvmainurl().'de/node/40">Beratungsstellen</a>';
	}
}
function atelierPreCrumb($lang){
	
	if($lang == 'fr'){
		echo '<a href="'.sbvmainurl().'fr/homefr">FSA</a> » <a href="'.sbvmainurl().'fr/node/118">Centres de formation et de rencontre</a>';
	} else {
		echo '<a href="'.sbvmainurl().'de">SBV</a> » <a href="'.sbvmainurl().'de/node/46">Bildungs- &amp; Begegnungszentren</a>';
	}
}
function sektionPreCrumb($lang){
	
	if($lang == 'fr'){
		echo '<a href="'.sbvmainurl().'fr/homefr">FSA</a> » <a href="'.sbvmainurl().'fr/node/99">Sections</a>';
	} else {
		echo '<a href="'.sbvmainurl().'de">SBV</a> » <a href="'.sbvmainurl().'de/node/53">Sektionen</a>';
	}
}

/* Put Breadcrumbs in a ul li structure */
function sbvfsa_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $breadcrumb = array_unique($breadcrumb);
   
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $crumbs = '<div class="monkee breadcrumb">';

    $array_size = count($breadcrumb);
    $i = 0;
    while ( $i < $array_size) {
   
      $pos = strpos($breadcrumb[$i], drupal_get_title());
      //we stop duplicates entering where there is a sub nav based on page jumps
      if ($pos === false){
        $crumbs .= '<span class="breadcrumb-' . $i;
        $crumbs .=  '">' . $breadcrumb[$i] . '</span> &gt; ';
      }
      $i++;
    }
    $crumbs .= '<span class="active">'. drupal_get_title() .'</span></div>';
    return $crumbs;
  }
}


function sbvfsa_preprocess_page(&$vars) {
	if (isset($vars['node']->type)) {
		$nodetype = $vars['node']->type;
		$vars['theme_hook_suggestions'][] = 'page__' . $nodetype;
	}
}

// monkee helper Functions
function selfURL() { 
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
	$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
	return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI']; 
} 

function strleft($s1, $s2) { 
	return substr($s1, 0, strpos($s1, $s2)); 
}


function readspeakLang() {
  // I added this sanity check
  if(arg(0) == 'node') {
    $nid = arg(1);
    $node = node_load($nid);
    $readspeaker = field_get_items('node', $node, 'field_readspeaker');
   
    if ($readspeaker != NULL) {
      $termid = $readspeaker[0]['tid'];
      //var_dump($termid);
      $terms = taxonomy_term_load($termid);
      //var_dump($terms);
       $readlang = $terms->name;

      if ($readlang == 'Franzoesisch'){
	$readlang = 'fr';
      } else if($readlang == 'none') {
	// none choosen, leave it that way
	$readlang = 'none';
      } else {
	// default -- German
	$readlang = 'de';
      }


    } else {
      // Which means there is nothing configured for field_readspeaker
      $readlang = 'nothing';
      // no result
    }

    return $readlang;
  }
}

function useReadSpeakLang(){
  global $language;

    $readLang = readspeakLang();
    if ($readLang == 'none') {
         // Explicitly turned OFF Readspeaker!
         return false;
    } else if ($readLang == 'nothing'){
	 // No Language found, look for system language
        if(arg(0) == 'node') {
	    $nid = arg(1);
	    $node = node_load($nid);
	    $sysLang =  field_get_items('node', $node, 'language');
	    // Thank you drupa to save german either as de, d, de_DE ant whatnot!
	    if ($sysLang == 'd'){
	        $sysLang = 'de';
	    } else if ($sysLang == 'f') {
	        $sysLang = 'fr';
	    }
	} else {
	  // In case we do not find node language, take the Systems/Env one...
	    $sysLang = $language->language;
	}
	
	 if($sysLang == 'und'){
	   // if Undefined (and nothing defined in readspeaker language field)
	   // turn off readspeaker!
	   return false;
	 } else {
	   return $sysLang;
	 }
    } else {
      // if it's not 'nothing' or 'none'
      // take the configured readspeaker language
      return readspeakLang();
    }
 
}
