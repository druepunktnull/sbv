<?php

function sbvmainurl(){
	return "http://wwwnew.sbv-fsa.ch/";
}

/**
 * Add body classes if certain regions have content.
 */
function bartik_preprocess_html(&$variables) {
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }

  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function bartik_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}


function monkee_source_domain_redirect($variables){
	// Monkee.ch
	// Fix redirect to source-domain
	global $_domain;

	// var_dump($_domain);die();
	//array(8) { ["domain_id"]=> string(1) "2" ["subdomain"]=> string(30) "bern.sektion.wwwnew.sbv-fsa.ch" ["sitename"]=> string(12) "Sektion Bern" ["scheme"]=> string(4) "http" ["valid"]=> string(1) "1" ["path"]=> string(38) "http://bern.sektion.wwwnew.sbv-fsa.ch/" ["site_grant"]=> bool(true) ["aliases"]=> array(0) { } } 
	$monkee_actual_domain_path = $_domain['path'];
	$monkee_actual_domain_name = $_domain['sitename'];
	
	$monkee_actual_domain_path_naked = str_replace("http://", "", $monkee_actual_domain_path);
	$monkee_actual_domain_path_naked = str_replace("/", "", $monkee_actual_domain_path_naked);
	
	
	$monkee_main_node_id = key($variables['page']['content']['system_main']['nodes']);
	
	  $domain = domain_get_node_match($monkee_main_node_id);
	
	$monkee_source_domain = $variables['page']['content']['system_main']['nodes'][$monkee_main_node_id]['body']['#object']->subdomains['1'];
	
	var_dump($monkee_actual_domain_path_naked ."   ".$monkee_source_domain);die();
	
	$monkee_source_domain_id = $variables['page']['content']['system_main']['nodes'][$monkee_main_nid_id]['body']['#object']->domain_source;
	
	  if ($monkee_actual_domain_path_naked != $monkee_source_domain && $monkee_source_domain != '') {
		$url = "http://". $monkee_source_domain;
		
	   drupal_goto($url);
	  }
	// END Monkee SOurce Domain Fix
}
/**
 * Override or insert variables into the page template.
 */
function bartik_process_page(&$variables) {
	
	// Monkee.ch
	// Fix redirect to source-domain
	global $_domain;
	global $_node;

	// var_dump($_domain);die();
	//array(8) { ["domain_id"]=> string(1) "2" ["subdomain"]=> string(30) "bern.sektion.wwwnew.sbv-fsa.ch" ["sitename"]=> string(12) "Sektion Bern" ["scheme"]=> string(4) "http" ["valid"]=> string(1) "1" ["path"]=> string(38) "http://bern.sektion.wwwnew.sbv-fsa.ch/" ["site_grant"]=> bool(true) ["aliases"]=> array(0) { } } 
	$monkee_actual_domain_path = $_domain['path'];
	$monkee_actual_domain_name = $_domain['sitename'];
	//var_dump($_domain);die();
	
	$monkee_actual_domain_path_naked = str_replace("http://", "", $monkee_actual_domain_path);
	$monkee_actual_domain_path_naked = str_replace("/", "", $monkee_actual_domain_path_naked);
	
	$monkee_main_node_id_key = key($variables['page']['content']['system_main']['nodes']); //ID of cuurent node
		
		
		
	
	$monkee_main_node = $variables['page']['content']['system_main']['nodes'][$monkee_main_node_id_key];
			

	if(is_array($monkee_main_node) && array_key_exists('body',$monkee_main_node)){
		
			
		$monkee_main_node_id = $monkee_main_node[$monkee_main_node_id_key]['body']['#object']->tnid;

		$domain = domain_get_node_match($monkee_main_node_id_key);

		$monkee_source_domain = $variables['page']['content']['system_main']['nodes'][$monkee_main_node_id]['body']['#object']->subdomains['1'];
		if(!$monkee_source_domain) {
			$monkee_source_domain = $variables['page']['content']['system_main']['nodes'][$monkee_main_node_id]['body']['#object']->subdomains['0'];
		}
		//var_dump($variables);die();

		//var_dump($variables['page']['content']['system_main']['nodes'][$monkee_main_node_id]['body']['#object']);die();
		//var_dump($monkee_actual_domain_path_naked ."   ".$monkee_source_domain);die();
		//var_dump($monkee_source_domain);die();

		$monkee_source_domain_id = $variables['page']['content']['system_main']['nodes'][$monkee_main_nid_id]['body']['#object']->domain_source;

	
		if ($monkee_actual_domain_path_naked != $monkee_source_domain 
			&& $monkee_source_domain && strpos($monkee_source_domain, '.')) {
				// Actually sometimes i get the Domain name instead of url... that's shit
	
				$url = "http://". $monkee_source_domain."".$_SERVER["REQUEST_URI"];
	
		   		drupal_goto($url);
		}
	}
	// END Monkee SOurce Domain Fix	
	
	
	// Monkee.ch
	// Homepage language redirect fix
	global $language;
	$monkee_active_language = $language->language;
	$monkee_lang_node = $variables['page']['content']['system_main']['nodes'][$monkee_main_node_id_key];
	
	
	if(is_array($monkee_lang_node) && array_key_exists('body',$monkee_lang_node)){
	
		$monkee_node_language = $monkee_lang_node['body']['#language'];
	
		//var_dump($monkee_active_language."  ".$monkee_node_language);die();
	
		if ($monkee_active_language != $monkee_node_language && $monkee_active_language){
	//		var_dump(translation_node_get_translations($monkee_main_node_id));die();
			$monkee_available_translations = translation_node_get_translations($monkee_main_node_id);
			//VAR_DUMP($monkee_available_translations);DIE();

			if ($monkee_available_translations[$monkee_active_language]){
				//VAR_DUMP('YAY');DIE();
				$monkee_nid = $monkee_available_translations[$monkee_active_language]->nid;
				//$_SESSION['currentlanguage'] = $monkee_node_language;
				$monkee_correct_lang_url = "node/".$monkee_nid;
				drupal_goto($monkee_correct_lang_url);
			} else {
				$monkee_lang_redirect = false;
				//CHANGE ENVIRONMENT TO NODE LANGUAGE THEN!
			}
		
		}
	}
	//var_dump(translation_node_get_translations($monkee_main_node_id));die();
	// End Monkee language redirect fix
	
	
	
	
	
	
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function bartik_preprocess_maintenance_page(&$variables) {
  if (!$variables['db_is_active']) {
    unset($variables['site_name']);
  }
  drupal_add_css(drupal_get_path('theme', 'bartik') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function bartik_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function bartik_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}

/**
 * Override or insert variables into the block template.
 */
function bartik_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function bartik_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function bartik_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' . $output . '</div>';

  return $output;
}


// Hide untranslated link...
function bartik_links__locale_block($variables) {
  foreach($variables['links'] as $key => $value) {
    if ($value['attributes']['class']=='locale-untranslated') {
      unset($variables['links'][$key]);
    }
  }
  return theme('links', $variables);
}


// Special Pre BreadCrumbs

function beratungPreCrumb($lang){
	
	if($lang == 'fr'){
		echo '<a href="'.sbvmainurl().'fr/homefr">FSA</a> » <a href="'.sbvmainurl().'fr/node/114">Services de consultation</a>';
	} else {
		echo '<a href="'.sbvmainurl().'de">SBV</a> » <a href="'.sbvmainurl().'de/node/40">Beratungsstellen</a>';
	}
}
function atelierPreCrumb($lang){
	
	if($lang == 'fr'){
		echo '<a href="'.sbvmainurl().'fr/homefr">FSA</a> » <a href="'.sbvmainurl().'fr/node/118">Ateliers</a>';
	} else {
		echo '<a href="'.sbvmainurl().'de">SBV</a> » <a href="'.sbvmainurl().'de/node/46">Ateliers</a>';
	}
}
function sektionPreCrumb($lang){
	
	if($lang == 'fr'){
		echo '<a href="'.sbvmainurl().'fr/homefr">FSA</a> » <a href="'.sbvmainurl().'fr/node/99">Sections</a>';
	} else {
		echo '<a href="'.sbvmainurl().'de">SBV</a> » <a href="'.sbvmainurl().'de/node/53">Sektionen</a>';
	}
}



function bartik_preprocess_page(&$vars) {
	if (isset($vars['node']->type)) {
		$nodetype = $vars['node']->type;
		$vars['theme_hook_suggestions'][] = 'page__' . $nodetype;
	}
}
