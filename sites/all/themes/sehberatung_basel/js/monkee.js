jQuery(document).ready(function(){
    
    /*** Login Form */
    jQuery('#block-user-login h2').replaceWith('<a class="loginLink" href="/user">Login</a>');
    jQuery('#block-user-login a.loginLink').click(function(event){
        event.preventDefault()	
        jQuery('#block-user-login .content').toggle();
    });



    /*** Search Field Hint */
jQuery.fn.hint = function (blurClass) {
  if (!blurClass) { 
    blurClass = 'blur';
  }

  return this.each(function () {
    // get jQuery version of 'this'
    var $input = jQuery(this),

    // capture the rest of the variable to allow for reuse
      title = $input.attr('title'),
      $form = jQuery(this.form),
      $win = jQuery(window);

    function remove() {
      if ($input.val() === title && $input.hasClass(blurClass)) {
        $input.val('').removeClass(blurClass);
      }
    }

    // only apply logic if the element has the attribute
    if (title) { 
      // on blur, set value to title attr if text is blank
      $input.blur(function () {
        if (this.value === '') {
          $input.val(title).addClass(blurClass);
        }
      }).focus(remove).blur(); // now change all inputs to title

      // clear the pre-defined text when form is submitted
      $form.submit(remove);
      $win.unload(remove); // handles Firefox's autocomplete
    }
   });
  };
  
  jQuery('#edit-search-block-form--2').hint();

	/* Hide Stuff from ReadSpeaker */
jQuery('div.field-name-field-sectionheader').remove();
jQuery('div.field-name-field-color').remove();

});
