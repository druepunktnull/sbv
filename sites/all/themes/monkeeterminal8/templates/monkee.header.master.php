<!-- MASTER header Copy from page--homepage.tpl.php! -->
<!-- Secondary Navigation -->
  <div class="container-fluid secondarynav">
    <div class="container">
        <!-- language -->
        <div class="language col-md-2 col-sm-12 col-xs-12">
            <?php print render($page['tagline_tools']); ?>
        </div>
        
        <!-- Access Tools -->
        <div class="access col-md-2 col-sm-12 col-xs-12">
            <ul class="accesstools">
            <!-- Access Tools -->
            <?php 
                if($language->language == 'fr') {
                echo'<li><a aria-hidden="true" onclick="changeLeftStyle(); return false;" title="affichage loupe" class="element-focusable" href="#"><i class="fa fa-fw fa-arrows-alt"></i></a></li>
                <li><a aria-hidden="true" class="increaseFont element-focusable zoomicon" onclick="resizeText(1); return false;" href="#" title="agrandir l\'écriture"><i class="fa fa-fw fa-plus-square"></i></i></a></li>
                <li><a aria-hidden="true" class="decreaseFont element-focusable zoomicon" onclick="resizeText(-1); return false;" href="#" title="réduire l\'écriture"><i class="fa fa-fw fa-minus-square"></i></a></li>
                <li><a aria-hidden="true" onclick="changeStyle(); return false;" title="changer le contraste" class="element-focusable" href="#"><i class="fa fa-fw fa-adjust"></i></a></li>';

                } else {
                echo'<li><a aria-hidden="true" onclick="changeLeftStyle(); return false;" title="Lupenansicht" class="element-focusable" href="#"><i class="fa-fw fa fa-arrows-alt"></i></a></li>
                <li><a aria-hidden="true" class="increaseFont element-focusable zoomicon" onclick="resizeText(1); return false;" href="#" title="Schrift vergrössern"><i class="fa-fw fa fa-plus-square"></i></a></li>
                <li><a aria-hidden="true" class="decreaseFont element-focusable zoomicon" onclick="resizeText(-1); return false;" href="#" title="Schrift verkleinern"><i class="fa-fw fa fa-minus-square"></i></a></li>
                <li><a aria-hidden="true" onclick="changeStyle(); return false;" title="Kontrast ändern" class="element-focusable" href="#"><i class="fa-fw fa fa-adjust"></i></a></li>';
                } 
            ?>
            </ul>
        </div>

        <!-- Search -->
        <div role="search" class="search col-md-4 col-sm-12 col-xs-12"><?php print render($page['search']); ?><i class="fa fa-search"></i></div>        
        
        <!-- Contact / Spenden -->
        <div class="contact col-md-4 col-sm-12 col-xs-12">
            <?php if($language->language == 'fr') { ?>
            <span><i class="fa fa-phone"></i> +41 21 651 60 60</span>
            <?php } else { ?>
            <span><i class="fa fa-phone"></i> +41 31 390 88 00</span>
            <?php } ?>
        </div>
       <?php
         if($language->language == 'fr') {
           $url_spenden = "/dons";
         } else {
           $url_spenden = "/spenden";
         }
       ?>
       
        <div class="spenden col-md-2 col-sm-12 col-xs-12">
            <a role="button" href="<?php echo $url_spenden; ?>"><i class="fa fa-heart"></i> <?php print t('Spenden'); ?></a>
        </div>

       
    <!-- Close Secondary Navigation -->   
    </div>
  </div>
  
  

    <div class="banner" role="banner">
      <div class="container">
      
        <!-- Logo -->
        <div class="navbar-header col-md-7 col-xs-12">
          <a class="" href="/">
            <?php if($language->language == 'fr') { ?>
               <div class="toplogo"><img alt="Logo de la FSA" src="/sites/all/themes/monkeeterminal8/front/img/sbv_logo_100_fr.jpg"></img></div>
            <?php } else { ?>
               <div class="toplogo"><img alt="Logo des SBV" src="/sites/all/themes/monkeeterminal8/front/img/sbv_logo_100_de.jpg"></img></div>
            <?php } ?>
          </a>
        </div>
        
        <!-- Claim -->
        <div class="col col-md-5 tagline">
            <?php print render($page['tagline']); ?>
        </div>
       
     </div>
    </div>
<!-- End MASTER header2 Copy from page--homepage.tpl.php! -->
