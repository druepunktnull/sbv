<!-- HEADER -->
<?php include 'monkee.header.master.php'; ?>



<?php 
  /* Initialize Variables */
  $list_start = 0;
  $list_range = 15;
  if ($_GET["start"]){
    $list_start = $_GET["start"];
  }
  if ($_GET["range"]){
    $list_range = $_GET["range"];
  }
?>




	




  <div id="main-wrapper container" class="clearfix"><div id="main" class="clearfix container">

    <?php if ($messages): ?>
      <div id="messages"><div class="section clearfix">
        <?php print $messages; ?>
      </div></div> <!-- /.section, #messages -->
    <?php endif; ?>

    <?php if ($breadcrumb): ?>
        <div id="breadcrumb" class="col-md-12"><?php print $breadcrumb; ?></div>
    <?php endif; ?>
      
    <div class="col-md-3 main-navigation" role="navigation">
	      <?php print render($page['sidebar_first']); ?>
	  </div>
	  
	  <div class="section col-md-9" id="content" role="content"><!-- Correct? -->
   
      <?php print render($title_prefix); ?>
      <?php if ($title): ?></i>
        <h1 name="top" class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      
     

	<!-- READ Speak Controls -->
	  <?php 
	if(useReadSpeakLang()){
	  if(useReadSpeakLang() == 'fr') {
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=fr_fr&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Ecoutez"> <span class="readspeakerbutton fr"></span></a> </div> <div id=\'xp1\'></div> ';	
		
	  } else if(useReadSpeakLang() == 'de'){
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=de_de&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Vorlesen"> <span class="readspeakerbutton de"></span></a> </div> <div id=\'xp1\'></div> ';
	  } 
	}
	?>

<div id="readoutloud" class="">
	  <div id="webformerror"></div>
	    <?php print render($page['help']); ?>
	      <?php print render($page['content']); ?>
	      
	      
	      


            <!-- NEWS out of Drupal Backend -->
            <div class="news-list produkt-list category" role="main">
               <?php
               // Daten		   
		              $query = new EntityFieldQuery();
		              $hasresults = false;
		              // Kategorie?
		              if ($kategorie = $node->field_produkt_kategorie['und'][0]['tid']){
                     $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'sbv_produkt')->fieldCondition('field_produkt_kategorie', 'tid', $kategorie)->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_sortier_datum', 'value', 'DESC');
                  } else {
		                $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'sbv_produkt')->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_sortier_datum', 'value', 'DESC');
		              }
		              
		              $result = $query->range($list_start,$list_range)->execute();
	             ?>   
           
           
           
                <?php // Loop
                  if (!empty($result['node'])) {
                    $nids = array_keys($result['node']);
                    $hasresults = true;

                  foreach ($nids as $nid) {
                    $produkt = node_load($nid, NULL, TRUE);  
                ?>  
                  <!-- Produkt -->
                  <div class="produkt row col-md-12">   
                           
                    <div class="text col-md-8">
                      <a href="/node/<?php echo $nid ?>"><h2 class="title"><?php echo $produkt->title; ?></h2></a>

                      <?php echo $produkt->body[$language->language][0]['safe_summary']; ?>
                    </div>
                    <div class="col-md-4">
                      <img src="<?php echo file_create_url($produkt->field_bild['und'][0]['uri']); ?>" alt="<?php echo $produkt->field_bild['und'][0]['alt']; ?>"/>  
                    </div>
                  </div> <!-- END One Produkt Line -->
		            <?php 
		              
		              }
		            }
	              ?>
           </div>

	      
	      
	       
	  </div><!-- readOutLoud -->
    <div class="pagination col-md-12">
    <?php

      // Pagination
      if ($list_start > 0){
        echo '<a href="?start=0&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Erste Seite") .' (1 - '.$list_range.')</button></a>';
      }
      if ($list_start > 0){
        $sum = $list_start - $list_range;
        $previous_start = 0;
        if ($sum > 0){
          $previous_start = $sum;
          echo '<a href="?start='. $previous_start .'&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Vorherige Seite") .' ('.$previous_start.' - '.$list_start.')</button></a>';
        }
      }
      $next_start = $list_start + $list_range;
      $next_end = $next_start + $list_range;
      echo '<a href="?start='. $next_start .'&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Nächste Seite") .' ('.$next_start.' - '.$next_end.')</button></a>'
      
    ?>	      
    </div>  
	      
	      
</div> <!-- /#content -->







    <?php if (false && $page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>


</div></div> <!-- /#main, /#main-wrapper -->









<!-- ADMIN STUFF -->
  <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
 

  
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>
	

	




  <?php include 'monkee.footer.master.php'; ?>

</div> <!-- /container -->


<!-- ADMIN Stuff -->
 <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>

  





<?php /*print render($page['content']); */
/*echo "<h1>START DEBUG DEBUG DEBUG</h1>";
var_dump(get_defined_vars());
echo "<h1>DEBUG DEBUG DEBUG END</h1>";*/
?>
