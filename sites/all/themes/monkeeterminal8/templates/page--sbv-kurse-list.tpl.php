<!-- HEADER -->
<?php include 'monkee.header.master.php'; ?>



<?php 
  /* Initialize Variables */
  $list_start = 0;
  $list_range = 18;
  if ($_GET["start"]){
    $list_start = $_GET["start"];
  }
  if ($_GET["range"]){
    $list_range = $_GET["range"];
  }
?>




	
  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, #messages -->
  <?php endif; ?>



  <div id="main-wrapper container" class="clearfix"><div id="main" class="clearfix container">

    <?php if ($breadcrumb): ?>
        <div id="breadcrumb" class="col-md-12"><?php print $breadcrumb; ?></div>
    <?php endif; ?>
      
    <div class="col-md-3 main-navigation" role="navigation">
	      <?php print render($page['sidebar_first']); ?>
	  </div>
	  
	  <div class="section col-md-9" id="content" role="content"><!-- Correct? -->
   
      <?php print render($title_prefix); ?>
      <?php if ($title): ?></i>
        <h1 name="top" class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      
     

	<!-- READ Speak Controls -->
	  <?php 
	if(useReadSpeakLang()){
	  if(useReadSpeakLang() == 'fr') {
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=fr_fr&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Ecoutez"> <span class="readspeakerbutton fr"></span></a> </div> <div id=\'xp1\'></div> ';	
		
	  } else if(useReadSpeakLang() == 'de'){
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=de_de&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Vorlesen"> <span class="readspeakerbutton de"></span></a> </div> <div id=\'xp1\'></div> ';
	  } 
	}
	?>

<div id="readoutloud" class="">
	  <div id="webformerror"></div>
	    <?php print render($page['help']); ?>
	      <?php print render($page['content']); ?>
	      
	      
	      
	      <!-- SUCHFORMULAR -->
	     <!-- <div class="customsearchform">
                <p><?php echo t('Ich suche einen Kurs'); ?></p>
                <form action="/search/node" method="get">
                    <input type="text" name="keys"><i class="fa fa-search"></i>
                    <input type="hidden" value="kurs" name="type">
                </form> 
	      </div> -->
	      


              <!-- Kurse out of Drupal Backend -->
              <div class="news-list category" role="main">
              <!-- Aktuelles -->
                 <div class="news">
                 
                 
                 <?php
                   $query = new EntityFieldQuery();
                        
                   // Kategorie? Query
                   if ($kategorie = $node->field_kategorie_kurs['und'][0]['tid']){
                     $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'kurs')->fieldCondition('field_kategorie_kurs', 'tid', $kategorie)->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_datum_kurs', 'value', 'ASC');;
                  } else {
                     $query->entityCondition('entity_type', 'node')->entityCondition('bundle','kurs')->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_datum_kurs', 'value', 'ASC');;
	                }
	                //$result = $query->range($list_start,$list_range)->execute();
	                $result = $query->execute();
	            ?>   
	            
                 <?php
                  // Resultat, Titel und Kurs-loop
                  if (!empty($result['node'])) {
		            $nids = array_keys($result['node']);
		            echo '<div class="news col-md-12"><h1>';
		            echo t('Nächste Kurse');
		            echo '</h1></div>';
		            
 		            echo '<div class="news col-md-12">';
 		            $display = array();
 	              print render(field_view_field('node', $node, 'field_einf_hrung_kursliste',$display));
		            echo '</div>'; ?>
                  <div class="news col-md-12">
                    <ul>
                 <?php
                 $monthtitle = "For the Month Group Titles!";
                 
                    foreach ($nids as $nid) {
                      $kurs = node_load($nid, NULL, TRUE);
                  ?>     
<?php
// Print month Titles
		$month = render(field_view_field("node", $kurs, 'field_datum_kurs', array(
  		'type' => 'date_default', // Field Formatter 
  		'label' => 'hidden', // Don't want to display label.
  		'settings' => array(
  	    'format_type' => 'monat', // date format like short, long or custom 
		  ),
		)));
		similar_text($month,$monthtitle,$percent);

		if ($percent < 98)  {
			echo "<h2>".$month."</h2>";	
			echo '<div class="line"></div>';
			$monthtitle = $month;	  
		}
?>

                    <li>

                       <a href="/node/<?php echo $nid ?>"><?php echo $kurs->title; ?></a>

                       <p class="text">

                         <?php 
                            echo $kurs->field_untertitel['und'][0]['value']; 
                            //$display = array();
                            //print render(field_view_field('node', $kurs, 'field_datum_kurs', $display));
                         ?>
                       </p>
                                       <div class="line"></div>
                       </li>
                       
                <?php 
                
                    }
                }
                ?>
                 </ul>

                    </div>
		

                    </div>

               
                </div>
	      
	      
	      
	      
	      
	  </div><!-- readOutLoud -->


<div style="display:none">
<?php
  // Pagination
  if ($list_start > 0){
    echo '<a href="?start=0&range='.$list_range.'"><button class="btn btn-default" type="button">Erste Seite (1 - '.$list_range.')</button></a>';
  }
  if ($list_start > 0){
    $sum = $list_start - $list_range;
    $previous_start = 0;
    if ($sum > 0){
      $previous_start = $sum;
      echo '<a href="?start='. $previous_start .'&range='.$list_range.'"><button class="btn btn-default" type="button">Vorherige Seite ('.$previous_start.' - '.$list_start.')</button></a>';
    }
  }
  $next_start = $list_start + $list_range;
  $next_end = $next_start + $list_range;
  echo '<a href="?start='. $next_start .'&range='.$list_range.'"><button class="btn btn-default" type="button">Nächste Seite ('.$next_start.' - '.$next_end.')</button></a>'
?>
</div>

   
</div></div> <!-- /.section, /#content -->







    <?php if (false && $page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>

  </div></div> <!-- /#main, /#main-wrapper -->









<!-- ADMIN STUFF -->
  <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
 

  
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>
	

	




  <?php include 'monkee.footer.master.php'; ?>


</div> <!-- /container -->


<!-- ADMIN Stuff -->
 <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>

<?php /*print render($page['content']); */
/*echo "<h1>START DEBUG DEBUG DEBUG</h1>";
var_dump(get_defined_vars());
echo "<h1>DEBUG DEBUG DEBUG END</h1>";*/
?>
