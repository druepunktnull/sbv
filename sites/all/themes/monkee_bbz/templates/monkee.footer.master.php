 <!-- Footer  MASTER page.tpl.php -->
    <div style="clear:both;"></div>
    <div class="container">
        <a href="#top" aria-hidden="true"><?php print t('Zurück nach oben') ?></a>
    </div>
    
    <footer class="container-fluid" id="Footer">
	
	<div class="footermenu container" role="navigation">
          <h2 class="header"><?php print t('Der SBV - schweizweit für Sie da') ?></h2>
	      <div class="row">

	        <div class="col col-md-4 col-sm-12 col-xs-12">
	            <?php print render($page['footer_firstcolumn']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-12 col-xs-12">
	            <?php print render($page['footer_secondcolumn']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-12 col-xs-12">
	            <?php print render($page['footer_thirdcolumn']); ?> 
	        </div>
	      </div>
	      

	      
	    </div>

	  </footer>
	  
	  <div style="clear:both;"></div>
	  
	  
      <!-- Weisser Balken zwischen Footer und Login --> 
      <div class="container" style="display:none">
	  	  <a href="#top" aria-hidden="true"><?php print t('Nach oben') ?></a>
      </div>
      <div style="clear:both;"></div>
      <div class="container logos" style="display:none">
	        <div class="row">
	          <div class="col col-md-12 col-sm-12 col-xs-12">
	          <?php print render($page['logo_line']); ?>
            </div>
	        </div>
       </div>
       <!-- END Weisser Balken zwischen Footer und Login --> 

       <div style="clear:both;"></div>


        <div class="container-fluid intern" id="Login">
	        <div class="container">
		      <div class="row">
		        <div class="col col-md-6 col-sm-6 col-xs-12"> 
		          <?php print render($page['intern_first']); ?>
		        </div>
		        <div class="col thirdnav col-md-6 col-sm-6 col-xs-12">
		          <div class="col-md-12 logos">
		            <?php if($language->language == 'fr') { 
		              $alt_berufsbildung = "Vers le site formationprofessionnelleplus.ch";
		              $url_berufsbildung = "http://www.berufsbildungplus.ch/fr/berufsbildungplus/berufsbildung";
		              $alt_swisslabel = "Vers le site de Swiss Label - Société pour la promotion des produits et services suisses";
		              $url_swisslabel = "http://www.swisslabel.ch/fr"; 
		            } else {
		              $alt_berufsbildung = "Zur Website berufsbildungplus.ch";
		              $url_berufsbildung = "http://www.berufsbildungplus.ch/";
		              $alt_swisslabel = "Zur Website von Swiss Label - Gesellschaft zur Promotion von Schweizer Produkten und Dienstleistungen";
		              $url_swisslabel = "http://www.swisslabel.ch/";
		            } 
		            ?>
		            
		            <a href="<?php echo $url_berufsbildung ?>" title="<?php echo $alt_berufsbildung ?>"><img src="/sites/all/themes/monkeeterminal8/logos/logo-berufsbildung-2x.png"/></a>
                <a href="<?php echo $url_swisslabel ?>" title="<?php echo $alt_swisslabel ?>"><img src="/sites/all/themes/monkeeterminal8/logos/logo-swisslabel-2x.png"/></a>
              </div>
              <div class="col-md-12">
	  	          <span class="links"><a href="/impressum">Impressum</a> | &copy; 2016 <?php print t('SBV'); ?> </span>
  		          <?php print render($page['intern_last']); ?>
		          </div>
		        </div>
		      </div>
		    </div>
		    

		    
	    </div>

		<div class="container-fluid usermenu">
	      <div class="row">
	        <div class="col col-md-4 col-sm-4 col-xs-12">  
	          <?php print render($page['user_first']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12"> 
	          <?php print render($page['user_middle']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12"> 
	          <?php print render($page['user_last']); ?>
	        </div>
	      </div>
	    </div>
     <!-- END Footer  MASTER -->
