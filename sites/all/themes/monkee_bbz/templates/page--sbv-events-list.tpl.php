<!-- HEADER -->
<?php include 'monkee.header.master.php'; ?>



<?php 
  /* Initialize Variables */
  $list_start = 0;
  $list_range = 9;
  if ($_GET["start"]){
    $list_start = $_GET["start"];
  }
  if ($_GET["range"]){
    $list_range = $_GET["range"];
  }
?>




	
  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, #messages -->
  <?php endif; ?>



  <div id="main-wrapper container" class="clearfix"><div id="main" class="clearfix container">

    <?php if ($breadcrumb): ?>
        <div id="breadcrumb" class="col-md-12"><?php print $breadcrumb; ?></div>
    <?php endif; ?>
      
    <div class="col-md-3 main-navigation" role="navigation">
	      <?php print render($page['sidebar_first']); ?>
	  </div>
	  
	  <div class="section col-md-9" id="content" role="content"><!-- Correct? -->
   
      <?php print render($title_prefix); ?>
      <?php if ($title): ?></i>
        <h1 name="top" class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      
     

	<!-- READ Speak Controls -->
	  <?php 
	if(useReadSpeakLang()){
	  if(useReadSpeakLang() == 'fr') {
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=fr_fr&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Ecoutez"> <span class="readspeakerbutton fr"></span></a> </div> <div id=\'xp1\'></div> ';	
		
	  } else if(useReadSpeakLang() == 'de'){
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=de_de&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Vorlesen"> <span class="readspeakerbutton de"></span></a> </div> <div id=\'xp1\'></div> ';
	  } 
	}
	?>

<div id="readoutloud" class="">
	  <div id="webformerror"></div>
	    <?php print render($page['help']); ?>
	      <?php print render($page['content']); ?>
	      
	      
	      
	     <!-- Searchform --> 
	      
	      
	      
	      <div class="customsearchform">
                <p><?php print t('Ich suche eine Veranstaltung'); ?></p>
                <form action="/search/node" method="post" id="eventsearchbox">
                    <input type="text" name="keys" id="searchterms"><i class="fa fa-search"></i><br>
                    <input type="hidden" value="veranstaltung" name="type">
                </form> 
	      </div>



        <div id="result"></div>




          <!-- Veranstaltungen out of Drupal Backend -->
          <div class="news-list veranstaltungs-list category" role="main">
              <!-- Aktuelles -->
              <div class="news veranstaltungen">       
                    
                <?php
                // Get Events
		           $query_past = new EntityFieldQuery();
		           $query_future = new EntityFieldQuery();
		              
		              
		              // Kategorie? Query
		           if ($kategorie = $node->field_kategorie_veranstaltung['und'][0]['tid']){
                     $query_past->entityCondition('entity_type', 'node')->entityCondition('bundle', 'veranstaltung')->fieldCondition('field_kategorie_veranstaltung', 'tid', $kategorie)->propertyCondition('language', $language->language, '=')->fieldCondition('field_datum_event','value', date("Y-m-d H:i:s"), '<')->fieldOrderBy('field_datum_event', 'value', 'DESC');
                     
                     $query_future->entityCondition('entity_type', 'node')->entityCondition('bundle', 'veranstaltung')->fieldCondition('field_kategorie_veranstaltung', 'tid', $kategorie)->propertyCondition('language', $language->language, '=')->fieldCondition('field_datum_event','value', date("Y-m-d H:i:s"), '>=')->fieldOrderBy('field_datum_event', 'value', 'ASC');
                     
                  } else {
                     $query_past->entityCondition('entity_type', 'node')->entityCondition('bundle', 'veranstaltung')->propertyCondition('language', $language->language, '=')->fieldCondition('field_datum_event','value', date("Y-m-d H:i:s"), '<')->fieldOrderBy('field_datum_event', 'value', 'DESC');
                     $query_future->entityCondition('entity_type', 'node')->entityCondition('bundle', 'veranstaltung')->propertyCondition('language', $language->language, '=')->fieldCondition('field_datum_event','value', date("Y-m-d H:i:s"), '>=')->fieldOrderBy('field_datum_event', 'value', 'ASC');
                  }
		              
		              $result_past = $query_past->range($list_start,$list_range)->execute();
		              $result_future = $query_future->range($list_start,$list_range)->execute();
	                ?>   
	                
	                
	                
	               
	             
                  <?php
                  // Zukunft
                  if (!empty($result_future['node'])) {
		            $nids = array_keys($result_future['node']);
		            echo '<div class="news col-md-12"><h1>';
		            echo t('Kommende Veranstaltungen');
		            echo '</h1></div>';
                  
                    foreach ($nids as $nid) {
                      $veranstaltung = node_load($nid, NULL, TRUE);
                  ?>   
          
                  <div class="news col-md-12"> 

                     
                      <!-- Datum für Sehende -->
                      <div class="date" aria-hidden="true">
                         <?php
                                $date = new DateTime($veranstaltung->field_datum_event['und'][0]['value']);
                                if ($language->language == 'fr'){
                                    setlocale(LC_ALL, 'fr_CH.UTF8');
                                    $date = strftime('%d %B %Y', $date->getTimestamp());
                                } else {
                                    setlocale(LC_ALL, 'de_CH.utf8');
                                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                                }                
                                echo $date;
                                
                                if ($veranstaltung->field_datum_event['und'][0]['value2']) {
                                    $date2 = new DateTime($veranstaltung->field_datum_event['und'][0]['value2']);
                                     if ($language->language == 'fr'){
                                        setlocale(LC_ALL, 'fr_CH.UTF8');
                                        $date2 = strftime('%d %B %Y', $date2->getTimestamp());
                                    } else {
                                        setlocale(LC_ALL, 'de_CH.utf8');
                                        $date2 = strftime('%d. %B %Y', $date2->getTimestamp());
                                    }
                                if ($date != $date2) {
                                  echo " - " . $date2;
                                }           
                                
                                }
                                

                      	 ?>
                       </div>
                    
                    
                      <a href="/node/<?php echo $nid ?>"><h2 class="title"><?php echo $veranstaltung->title; ?></h2></a>
                       <!-- Datum für Screenreader -->
                       <div class="date ScreenReader">
                         <?php
                                $date = new DateTime($veranstaltung->field_datum_event['und'][0]['value']);
                                if ($language->language == 'fr'){
                                    setlocale(LC_ALL, 'fr_CH.UTF8');
                                    $date = strftime('%d %B %Y', $date->getTimestamp());
                                } else {
                                    setlocale(LC_ALL, 'de_CH.utf8');
                                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                                }                
                                echo $date;
                      	 ?>
                       </div>
                       <p class="text">
                         <?php
                            if ($veranstaltung->field_ort['und'][0]['value']) {
                               echo $veranstaltung->field_ort['und'][0]['value'] . "<br />"; 
                            }
                            //echo $veranstaltung->body[$language->language][0]['safe_summary']; 
                            echo $veranstaltung->field_untertitel['und'][0]['value']; 
                            //echo $veranstaltung->body[$language->language][0]['safe_summary']; 
                         ?>
                       </p>
                     </div>
                <?php 
                    }
                }
                ?>
                
                
                
                 
                  <?php
                  // Vergangenheit
                  if (!empty($result_past['node'])) {
		            $nids = array_keys($result_past['node']);
		            echo '<div class="news col-md-12"><h1>';
		            echo t('Vergangene Veranstaltungen');
		            echo '</h1></div>';
                  
                    foreach ($nids as $nid) {
                      $veranstaltung = node_load($nid, NULL, TRUE);
                  ?>   
          
                  <div class="news col-md-12"> 

                     
                      <!-- Datum für Sehende -->
                      <div class="date" aria-hidden="true">
                         <?php
                                $date = new DateTime($veranstaltung->field_datum_event['und'][0]['value']);
                                if ($language->language == 'fr'){
                                    setlocale(LC_ALL, 'fr_CH.UTF8');
                                    $date = strftime('%d %B %Y', $date->getTimestamp());
                                } else {
                                    setlocale(LC_ALL, 'de_CH.utf8');
                                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                                }                
                                echo $date;
                                
                                if ($veranstaltung->field_datum_event['und'][0]['value2']) {
                                    $date2 = new DateTime($veranstaltung->field_datum_event['und'][0]['value2']);
                                     if ($language->language == 'fr'){
                                        setlocale(LC_ALL, 'fr_CH.UTF8');
                                        $date2 = strftime('%d %B %Y', $date2->getTimestamp());
                                    } else {
                                        setlocale(LC_ALL, 'de_CH.utf8');
                                        $date2 = strftime('%d. %B %Y', $date2->getTimestamp());
                                    }
                                if ($date != $date2) {
                                  echo " - " . $date2;
                                }           
                                
                                }
                                

                      	 ?>
                       </div>
                    
                    
                      <a href="/node/<?php echo $nid ?>"><h2 class="title"><?php echo $veranstaltung->title; ?></h2></a>
                       <!-- Datum für Screenreader -->
                       <div class="date ScreenReader">
                         <?php
                                $date = new DateTime($veranstaltung->field_datum_event['und'][0]['value']);
                                if ($language->language == 'fr'){
                                    setlocale(LC_ALL, 'fr_CH.UTF8');
                                    $date = strftime('%d %B %Y', $date->getTimestamp());
                                } else {
                                    setlocale(LC_ALL, 'de_CH.utf8');
                                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                                }                
                                echo $date;
                      	 ?>
                       </div>
                       <p class="text">
                         <?php
                            if ($veranstaltung->field_ort['und'][0]['value']) {
                               echo $veranstaltung->field_ort['und'][0]['value'] . "<br />"; 
                            }
                            //echo $veranstaltung->body[$language->language][0]['safe_summary']; 
                            echo $veranstaltung->field_untertitel['und'][0]['value']; 
                            //echo $veranstaltung->body[$language-sbv-test-error.log>language][0]['safe_summary']; 
                         ?>
                       </p>
                     </div>
                <?php 
                    }
                }
                ?>
                
                        
                
                
                
                
            </div>
        </div>

	      
	      
	      
	      
	  </div><!-- readOutLoud -->








<!-- Search by Node-Type--> 
<script type="text/javascript">
   $("#eventsearchbox").submit(function(e){
    var form = $(this);
    var searchterm = $('#searchterms').val();
    $.post({ 
         url   : form.attr('action'),
         data  : { keys: searchterm + ' type:veranstaltung'}, // data to be submitted
         success: function(response){
            var content = $( response ).find( "ol.search-results");
            
            if (content.length !== 0) {
              $( "#result" ).empty().append( "<h1>Suchergebnisse</h1>");
              $( "#result" ).append(content);
              $( "div.veranstaltungs-list" ).hide();
            } else {
              $( "#result" ).empty().append( "<h2>Keine Suchergebnisse!</h2>" );
              $( "div.veranstaltungs-list" ).show();
            }
            
         }
    });
    return false;
 });
 
 /*  div.veranstaltungslist  */
</script>







<?php
  // Pagination
  if ($list_start > 0){
    echo '<a href="?start=0&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Erste Seite") .' (1 - '.$list_range.')</button></a>';
  }
  if ($list_start > 0){
    $sum = $list_start - $list_range;
    $previous_start = 0;
    if ($sum > 0){
      $previous_start = $sum;
      echo '<a href="?start='. $previous_start .'&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Vorherige Seite") .' ('.$previous_start.' - '.$list_start.')</button></a>';
    }
  }
  $next_start = $list_start + $list_range;
  $next_end = $next_start + $list_range;
  echo '<a href="?start='. $next_start .'&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Nächste Seite") .' ('.$next_start.' - '.$next_end.')</button></a>'
?>
	      
	      
</div></div> <!-- /.section, /#content -->







    <?php if (false && $page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>

  </div></div> <!-- /#main, /#main-wrapper -->









<!-- ADMIN STUFF -->
  <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
 

  
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>
	

	




  <?php include 'monkee.footer.master.php'; ?>

</div> <!-- /container -->


<!-- ADMIN Stuff -->
 <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>

  





<?php /*print render($page['content']); */
/*echo "<h1>START DEBUG DEBUG DEBUG</h1>";
var_dump(get_defined_vars());
echo "<h1>DEBUG DEBUG DEBUG END</h1>";*/
?>
