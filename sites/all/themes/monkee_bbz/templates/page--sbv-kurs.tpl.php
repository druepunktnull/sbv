<?php

/**
 *
 * monkee.ch
 *
 *** Single Course Template ***
 * @author manfred@monkee.ch
 *
 */
?>


<?php include 'monkee.header.master.php'; ?>

    
  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>






<div class="container news-list" role="main">
<!-- Aktuelles -->

    <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

  <div class="row">
	 
      <div class="col-md-3" role="navigation">
		  <?php print render($page['sidebar_first']); ?>
      </div>
    <div class="news col-md-9" id="content">
		
	  <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
		
    	<h1 class="title"><?php echo $node->title; ?> </h1>

     	<p>
     	
     	  </p>
     	  
     	  
     	 
     	  
     	  
     	  
     	  
     	  
     	  <span class="date_fine">
     	  <?php
                $date = new DateTime($node->field_datum['und'][0]['value']);
                if ($language->language == 'fr'){
                    setlocale(LC_ALL, 'fr_CH.UTF8');
                    $date = strftime('%d %B %Y', $date->getTimestamp());
                } else {
                    setlocale(LC_ALL, 'de_CH.utf8');
                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                }                
                echo $date;
     	  ?>
     	</span>

     	</p>
      <p>
        <span class="teaser"><b>

        </span></b>
      </p>
      <p class="text">
        <?php echo $node->body[$language->language][0]['value']; ?>
      </p>

      <div class="assets">
		  <?php if ($node->field_downloads['und']) { ?>
        <h2><?php print t('Downloads') ?></h2>
          <ul>
          <?php 
            foreach ($node->field_downloads['und'] as $download) {
              echo '<li><a href="';
              echo file_create_url($download['uri']);
              echo '">';
              echo $download['description'];
              echo '</a></li>';
            }
          ?>
          </ul>
          <?php } ?>

        <?php if ($node->field_links['und']) { ?>
        <h2><?php print t('Links') ?></h2>
        <ul>
          <?php 
            foreach ($node->field_links['und'] as $links) {
              echo '<li><a href="';
              echo $links['url'];
              echo '">';
              echo $links['title'];
              echo '</a></li>';
            }
          ?>
          </ul>
          <?php } ?>
      </div> <!-- assets -->
      
     
    </div>
    
  </div>
  
</div>


 <?php include 'monkee.footer.master.php'; ?>



	  
	  
	  
	  
	  
	  





