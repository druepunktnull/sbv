<?php

/*
 * monkee.ch
 * TWS Kampagnen Seite 2016
 */
?>

<script>
	var pathname = window.location;
	if (pathname == "http://canne-blanche.info/de") {
	  window.location = "http://weisser-stock.info/de";
	}
	if (pathname == "http://weisser-stock.info/fr") {
	  window.location = "http://canne-blanche.info/fr";
	}
</script>


<!-- HEADER -->
<?php include 'monkee.header.master.php'; ?>


<div class="container">
 <p><b><?php echo $node->title; ?></b></p>

</div>

<!-- MAIN Button Menu -->
      <?php if($language->language == 'fr') { ?>
      
      
      <!-- Französisch -->
      <div class="container">
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    <nav class="" aria-controls="navbar" role="navigation" id="Menu">

        <ul class="nav navbar-nav">
        
					<div class="ScreenReader">
						<li>
							 <a href="/homefr">
							  <?php print t('A la site du FSA') ?>
							</a>
						</li>
					</div>

					<div class="row container">
						<div class="col-md-3 col-xs-12"><li>
							<a href="https://www.facebook.com/sbv.fsa ">
							  <?php print t('Publier un "risque"') ?>
							</a>
						</li></div>     

						<div class="col-md-3 col-xs-12"><li>
							<a href="/fr/communiques-de-presse">
							  <?php print t('Communiqué de presse') ?>
							</a>
						</li></div>      

						<div class="col-md-3 col-xs-12"><li>
							<a href="http://defensedesinterets.www.sbv-fsa.ch/fr/guidage_au_sol ">	  
							  <?php print t('Guidage au sol') ?>
							</a>
						</li></div>
						
						<div class="col-md-3 col-xs-12"><li>
							<a href="/fr/fsa_en_bref">
							  <img src="/sites/all/themes/monkeeterminal8/icons/icon-sbv-in-kuerze-2x.png"/> <?php print t('FSA en bref') ?>
							</a>
						</li></div>
					</div>

        </ul>
  	  </nav>
    </div>
  </div>
	
	<?php  } else { ?>
	   
	   <!-- Deutsch -->

	  <!-- Example row of columns -->
	  
 <div class="container">
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    <nav class="" aria-controls="navbar" role="navigation" id="Menu">

        <ul class="nav navbar-nav">
        
					<div class="ScreenReader">
						<li>
							 <a href="/node/81">
							  <?php print t('Zur normalen SBV Seite') ?>
							</a>
						</li>
					</div>

					<div class="row container">
						<div class="col-md-3 col-xs-12"><li>
							<a href="https://www.facebook.com/sbv.fsa ">
							 <?php print t('Gefahr posten') ?>
							</a>
						</li></div>     

						<div class="col-md-3 col-xs-12"><li>
							<a href="/de/medienmitteilungen">
							  <?php print t('Zur Medienmitteilung') ?>
							</a>
						</li></div>      

						<div class="col-md-3 col-xs-12"><li>
							<a href="http://interessenvertretung.www.sbv-fsa.ch/de/wegfuehrung">  
							  <?php print t('Wegführung') ?>
							</a>
						</li></div>
						
						<div class="col-md-3 col-xs-12"><li>
							<a href="/sbv_in_kuerze">
							  <img src="/sites/all/themes/monkeeterminal8/icons/icon-sbv-in-kuerze-2x.png"/> <?php print t('SBV in Kürze') ?>
							</a>
						</li></div>
					</div>

        </ul>
  	  </nav>
    </div>
  </div>

	
	
	<?php } ?>
<!-- END Main Menu -->


<div class="container bigpicture">
  <?php if($language->language == 'fr') { 

    $tagline = "De gauche à droite, la silhouette d'une personne au téléphone, une valise ainsi qu'un vélo se trouvent sur trois zones d'avertissement et bloquent ainsi l'accès aux lignes de guidage. Copyright FSA";
  } else {

    $tagline = "Von links nach rechts, die Silhouette einer Person am Telefon, eines Koffers und eines Fahrrads, welche drei Aufmerksamkeitsfelder von Leitlinien blockieren. Copyright SBV";
  }
  
  ?>
  <img width="1400px" height="440" alt="<?php echo $tagline; ?>" src="/sites/all/themes/monkeeterminal8/images/kampagnen/TWS_Header_2016.jpg"/>
</div>









<!-- Hauptinhalt -->
<div class="container news-list" role="main">
  	  

<!-- Donate -->
   <div class="call-to-actions col-md-4 col-sm-12 col-xs-12">
      <div class="logos donate col-md-12">		        
			        

               <div class="col-md-12">
                  <?php if($language->language == 'fr') { ?>
                    <h3><a href="/dons" aria-hidden="true"><?php print t('Spenden') ?></a></h3>
                  <?php } else {?>
                    <h3><a href="/spenden" aria-hidden="true"><?php print t('Spenden') ?></a></h3>
                  <?php } ?>
               </div>
               <div class="col-md-8"><p><?php print t('Unterstützen Sie') ?> <br /><?php print t('den SBV') ?></p></div>
               
               <?php if($language->language == 'fr') { 
		              $alt_zewo = "Vers le site de la Fondation ZEWO - Service suisse de certification pour les organisations d'utilité publique collectant des dons";
		              $url_zewo = "https://www.zewo.ch/fr";
		              $url_spenden = "/dons_en_ligne";
		            } else {
                      $alt_zewo = "Zur Website der Stiftung ZEWO - Schweizerische Zertifizierungsstelle für gemeinnützige Spenden sammelnde Organisationen";
		              $url_zewo = "https://www.zewo.ch/"; 
		              $url_spenden = "/spenden";
		            } 
		           ?>
               <div class="col-md-4"><a href="<?php echo $url_zewo; ?>" title="<?php echo $alt_zewo; ?>" ><img src="/sites/all/themes/monkeeterminal8/logos/logo-zewo-2x.png"/></a></div>
               <div class="col-md-12">
                 <p><a href="<?php echo $url_spenden; ?>" role="button"><i class="fa fa-heart"></i> <?php print t('Jetzt online spenden') ?></a></p>
               </div>


            
           
	    </div>
	    
	    <div class="newsletter col-md-12">
	    <?php if($language->language == 'fr') { ?>
            <a href="/newsletter_fsa">
	            <i class="fa fa-envelope"></i> <?php print t("S'abonner à la newsletter") ?>
	        </a>
        <?php } else { ?>
            <a href="/newsletter">
	            <i class="fa fa-envelope"></i> <?php print t('Newsletter abonnieren') ?>
	        </a> 
        <?php } ?>
	    
	     </div>
	</div>
	

	

	<div class="news col-md-7 col-sm-12 col-xs-12">
	 	<h2 class="header">
			<?php echo $node->title; ?>
   	</h2>
    <div class="row">

		<?php 
				if($language->language == 'fr') {
					$highlight_text = "À la une";
				} else {
					$highlight_text = "Highlight";
				}

				echo '<div class="news highlight col-md-12">';
				echo '<div class="highlightbatch"><i class="fa fa-star"></i> '. $highlight_text .'</div><div class="clear"></div>';
		?>  

     <a href="/node/<?php echo $nid ?>"><h2 class="title"><?php echo $news->title; ?></h2></a>
       
     <p class="text">
    	<?php echo $node->body[$language->language][0]['value']; ?>
     </p>
		</div>

	</div>
        
</div></div>
 
	





	  
	  
	  
 <!-- FOOTER -->	  
<?php include 'monkee.footer.master.php'; ?>





	</div> <!-- /container -->








 <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>

  





<?php /*print render($page['content']); */
/*echo "<h1>START DEBUG DEBUG DEBUG</h1>";
var_dump(get_defined_vars());
echo "<h1>DEBUG DEBUG DEBUG END</h1>";*/
?>
