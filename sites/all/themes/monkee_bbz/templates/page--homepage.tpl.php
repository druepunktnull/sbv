<?php

/**
 * @file
 * sbvfsa's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system folder.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or /themes//sbvfsa.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer_fifthcolumn']: Items for the fifth footer column.
 * - $page['footer_sixthcolumn']: Items for the sixth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see sbvfsa_process_page()
 */
?>


<!-- HEADER -->
<?php include 'monkee.header.master.php'; ?>


<!-- MAIN Button Menu -->
    
      <div class="container beratung">
	  <div class="container">
	    <nav class="" aria-controls="navbar" role="navigation" id="Menu">

        <ul class="nav navbar-nav">


          <div class="row container">
          
        <?php if ($node->field_men_links['und']) { ?>

          <?php 
            foreach ($node->field_men_links['und'] as $links) {
              echo '<div class="col-md-3 col-xs-12"><li><a href="';
              echo $links['url'];
              echo '">';
              echo $links['title'];
              echo '</a></li></div>  ';
            }
          ?>

          <?php } else { echo "Bitte Menülinks definieren!";}?>
          
		</div>
         
	          
	        </ul>
    	  </nav>
    </div>
  </div>
	

<!-- END Main Menu -->


<div class="container bigpicture">

  <img width="1400px" height="440" src="<?php echo file_create_url($node->field_bannerbild['und'][0]['uri']); ?>" alt="<?php echo $node->field_bannerbild['und'][0]['alt']; ?>"/>

</div>









<!-- NEWS out of Drupal Backend -->
<div class="container news-list" role="main">
  	  

<!-- Aktuelles -->

   <div class="call-to-actions col-md-4 col-sm-12 col-xs-12">
      <div class="logos donate col-md-12">		        
			        

               <div class="col-md-12">
                  <?php if($language->language == 'fr') { ?>
                    <h3><a href="/dons" aria-hidden="true"><?php print t('Spenden') ?></a></h3>
                  <?php } else {?>
                    <h3><a href="/spenden" aria-hidden="true"><?php print t('Spenden') ?></a></h3>
                  <?php } ?>
               </div>
               <div class="col-md-8"><p><?php print t('Unterstützen Sie') ?> <br /><?php print t('den SBV') ?></p></div>
               
               <?php if($language->language == 'fr') { 
		              $alt_zewo = "Vers le site de la Fondation ZEWO - Service suisse de certification pour les organisations d'utilité publique collectant des dons";
		              $url_zewo = "https://www.zewo.ch/fr";
		              $url_spenden = "/dons_en_ligne";
		            } else {
                      $alt_zewo = "Zur Website der Stiftung ZEWO - Schweizerische Zertifizierungsstelle für gemeinnützige Spenden sammelnde Organisationen";
		              $url_zewo = "https://www.zewo.ch/"; 
		              $url_spenden = "/spenden";
		            } 
		           ?>
               <div class="col-md-4"><a href="<?php echo $url_zewo; ?>" title="<?php echo $alt_zewo; ?>" ><img src="/sites/all/themes/monkeeterminal8/logos/logo-zewo-2x.png"/></a></div>
               <div class="col-md-12">
                 <p><a href="<?php echo $url_spenden; ?>" role="button"><i class="fa fa-heart"></i> <?php print t('Jetzt online spenden') ?></a></p>
               </div>


            
           
	    </div>
	    
	    <div class="newsletter col-md-12">
	    <?php if($language->language == 'fr') { ?>
            <a href="/newsletter_fsa">
	            <i class="fa fa-envelope"></i> <?php print t("S'abonner à la newsletter") ?>
	        </a>
        <?php } else { ?>
            <a href="/newsletter">
	            <i class="fa fa-envelope"></i> <?php print t('Newsletter abonnieren') ?>
	        </a> 
        <?php } ?>
	    
	     </div>
	</div>
	

	

   <div class="news col-md-7 col-sm-12 col-xs-12">
     <h2 class="header">
           <?php if($language->language == 'fr') { ?>
             Actualités
          <?php } else { ?>
             Aktuell
          <?php } ?>
     	</h2>
      <div class="row">
               <?php
		   
		              $query = new EntityFieldQuery();
		              
		              // Kategorie? Query + Publikaionsdate
		              if ($kategorie = $node->field_kategorie['und'][0]['tid']){
                     $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'sbv_news')->fieldCondition('field_kategorie', 'tid', $kategorie)->fieldCondition('field_publikation_ab', 'value', date("Y-m-d"), '<=')->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_datum', 'value', 'DESC');
                  } else {
		                $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'sbv_news')->fieldCondition('field_publikation_ab', 'value', date("Y-m-d"), '<=')->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_datum', 'value', 'DESC');
		              }
		              
		              $result = $query->range(0,3)->execute();
	             ?>   
    <?php
      if (!empty($result['node'])) {
		  $nids = array_keys($result['node']);

        foreach ($nids as $nid) {
            $news = node_load($nid, NULL, TRUE);
            $field_language = field_language('node', $news, 'field_date');
        ?>
        
        
<!-- One News Line -->
<?php 

    if($language->language == 'fr') {
      $highlight_text = "À la une";
    } else {
      $highlight_text = "Highlight";
    }
    if ($news->field_puplikationsoptionen['und'][0]['tid'] == 39){
        echo '<div class="news highlight col-md-12">';
        echo '<div class="highlightbatch"><i class="fa fa-star"></i> '. $highlight_text .'</div><div class="clear"></div>';
    } else {
        echo '<div class="news col-md-12">';
    }       
?>  

         <div class="date">
           <?php
              $date = new DateTime($news->field_datum['und'][0]['value']);
              if ($language->language == 'fr'){
                  setlocale(LC_ALL, 'fr_CH.UTF8');
                  $date = strftime('%d %B %Y', $date->getTimestamp());
              } else {
                  setlocale(LC_ALL, 'de_CH.utf8');
                  $date = strftime('%d. %B %Y', $date->getTimestamp());
              }                
              echo $date;
        	 ?>
        	 <?php 
              if ($news->field_puplikationsoptionen['und'][0]['tid'] == 40){
           	    // 40 ist Medienmitteilung
                echo '<span class="badge">Medienmitteilung</span>';
              }
           ?>
         </div>
           
         <a href="/node/<?php echo $nid ?>"><h2 class="title"><?php echo $news->title; ?></h2></a>
           
         <p class="text">

           <?php echo $news->field_anriss_text_teaser['und'][0]['value']; ?>
         </p>
       </div> <!-- END One News Line -->
		<?php 
		  
		  }
		 }
	  ?>
		

      </div>
      <?php if($language->language == 'fr') { ?>
        <a href="/actualites"><button type="button" class="btn btn-default"><b><?php print t('Toutes les actualités') ?></b></button></a>
      <?php } else { ?>     
        <a href="/aktuell"><button type="button" class="btn btn-default"><b><?php print t('Alle News anzeigen') ?></b></button></a>
      <?php } ?>
   </div>
 
	
</div>




	  
	  
	  
 <!-- FOOTER -->	  
<?php include 'monkee.footer.master.php'; ?>





	</div> <!-- /container -->








 <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>

  





<?php /*print render($page['content']); */
/*echo "<h1>START DEBUG DEBUG DEBUG</h1>";
var_dump(get_defined_vars());
echo "<h1>DEBUG DEBUG DEBUG END</h1>";*/
?>
