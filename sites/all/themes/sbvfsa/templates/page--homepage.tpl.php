<?php

/**
 * @file
 * sbvfsa's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system folder.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or /themes//sbvfsa.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see sbvfsa_process_page()
 */
?>

<script>
	var pathname = window.location;
	if (pathname == "http://sbv-fsa.ch/en") {
	  window.location = "http://www.sbv-fsa.ch/";
	}
</script>

	<!-- MASTER header2 Copy from here! -->
    <div class="container header2">
      <div class="col col-md-12 tagline">
         <?php print render($page['tagline']); ?>
      </div>
      <div class="col col-md-12 tools">
         
        <div class="language"><?php print render($page['tagline_tools']); ?></div>
        <div class="access">
           <ul class="accesstools">
           <!-- Access Tools -->
           <?php 
            if($language->language == 'fr') {
              echo'<li><a aria-hidden="true" onclick="changeLeftStyle(); return false;" title="affichage loupe" class="element-focusable" href="#"><i class="fa fa-caret-square-o-left"></i></a></li>
              <li><a aria-hidden="true" class="increaseFont element-focusable" onclick="resizeText(1); return false;" href="#" title="agrandir l\'écriture"><i class="fa fa-plus-square"></i></a></li>
              <li><a aria-hidden="true" class="decreaseFont element-focusable" onclick="resizeText(-1); return false;" href="#" title="réduire l\'écriture"><i class="fa fa-minus-square"></i></a></li>
              <li><a aria-hidden="true" onclick="changeStyle(); return false;" title="changer le contraste" class="element-focusable" href="#"><i class="fa fa-adjust"></i></a></li>';

            } else {
              echo'<li><a aria-hidden="true" onclick="changeLeftStyle(); return false;" title="Lupenansicht" class="element-focusable" href="#"><i class="fa fa-caret-square-o-left"></i></a></li>
              <li><a aria-hidden="true" class="increaseFont element-focusable" onclick="resizeText(1); return false;" href="#" title="Schrift vergrössern"><i class="fa fa-plus-square"></i></a></li>
              <li><a aria-hidden="true" class="decreaseFont element-focusable" onclick="resizeText(-1); return false;" href="#" title="Schrift verkleinern"><i class="fa fa-minus-square"></i></a></li>
              <li><a aria-hidden="true" onclick="changeStyle(); return false;" title="Kontrast ändern" class="element-focusable" href="#"><i class="fa fa-adjust"></i></a></li>';
            } 
           ?>
         </ul>
        </div>

        <div role="search" class="search"><i class="fa fa-search"></i><?php print render($page['search']); ?></div>        
      </div>
    </div>
	<!-- End MASTER header2 Copy from here! -->

<!-- MAIN Button Menu -->
      <?php if($language->language == 'fr') { ?>
      
      <!-- Französisch -->
	<div class="container MenuButtons" id="Menu">
	  <!-- Example row of columns -->
	  <nav aria-controls="navbar" role="navigation">
		  <div class="ScreenReader">
		      <a href="/aktuell">
		        <div class="panel text-center link3" role="button">
		          <h2><?php print t('Aktuell') ?></h2>
		        </div>
		      </a>
		  </div>
		  <div class="row">
		    <div class="MenuButton col col-md-4">
		      <a href="/conseil">
		        <div class="panel text-center link1" role="button">
		          <h2><i class="fa fa-question-circle" aria-hidden="true"></i><?php print t('Conseil') ?></h2>
		        </div>
		      </a>
		    </div>
		    <div class="MenuButton col col-md-4">
		      <a href="/cours_loisirs">
		        <div class="panel text-center link2" role="button">
		          <h2><i class="fa fa-users" aria-hidden="true"></i><?php print t('Cours & loisirs') ?></h2>
		        </div>
		      </a>
		    </div>
		    <div class="MenuButton col col-md-4">
		      <a href="/engagement">
		        <div class="panel text-center link3" role="button">
		          <h2><i class="fa fa-flag" aria-hidden="true"></i><?php print t('Engagement') ?></h2>
		        </div>
		      </a>
		    </div>
		    <div class="ScreenReader">
		      <a href="/dons">
		        <div class="panel text-center link3" role="button">
		          <h2><?php print t('faire un don') ?></h2>
		        </div>
		      </a>
		    </div>
		  </div>
	  </nav>
	</div>
	
	<?php  } else { ?>
	   
	   <!-- Deutsch -->
	   <div class="container MenuButtons" id="Menu">
	  <!-- Example row of columns -->
	  <nav aria-controls="navbar" role="navigation">
		  <div class="ScreenReader">
		      <a href="/aktuell">
		        <div class="panel text-center link3" role="button">
		          <h2><?php print t('Aktuell') ?></h2>
		        </div>
		      </a>
		  </div>
		  <div class="row">
		    <div class="MenuButton col col-md-4">
		      <a href="/beratung">
		        <div class="panel text-center link1" role="button">
		          <h2><i class="fa fa-question-circle" aria-hidden="true"></i><?php print t('Beratung') ?></h2>
		        </div>
		      </a>
		    </div>
		    <div class="MenuButton col col-md-4">
		      <a href="/kurse_freizeit">
		        <div class="panel text-center link2" role="button">
		          <h2><i class="fa fa-users" aria-hidden="true"></i><?php print t('Kurse & Freizeit') ?></h2>
		        </div>
		      </a>
		    </div>
		    <div class="MenuButton col col-md-4">
		      <a href="/engagement">
		        <div class="panel text-center link3" role="button">
		          <h2><i class="fa fa-flag" aria-hidden="true"></i><?php print t('Engagement') ?></h2>
		        </div>
		      </a>
		    </div>
		    <div class="ScreenReader">
		      <a href="/spenden">
		        <div class="panel text-center link3" role="button">
		          <h2><?php print t('Spenden') ?></h2>
		        </div>
		      </a>
		    </div>
		  </div>
	  </nav>
	</div>
	
	
	<?php } ?>










<!-- Highlight Article - for a primary marketing message or call to action -->

	<div class="container highlight" id="Content" aria-hidden="true">
	
	  <?php
		$query = new EntityFieldQuery();

        // TODO Live - Correct TID for Highlight!
		$query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'sbv_news')->fieldCondition('field_puplikationsoptionen', 'tid', 39)->propertyCondition('language', $language->language, '=');
		$result = $query->execute();
	  ?>

	  <?php
        if (empty($result['node'])) {
      ?>
            <div class="panel bigpicture">
	    	<img src="<?php print base_path() . path_to_theme();?>/front/img/default_header.jpg"></img>
	    </div>

       <?php
        } else {
		  $nids = array_keys($result['node']);

		  foreach ($nids as $nid) {
		    $highlight = node_load($nid, NULL, TRUE);
                    $field_language = field_language('node', $highlight, 'field_date');
        ?>
            <div class="panel article">
  		        <span class="badge"><?php print t('Top aktuell') ?></span>

			    <div class="row">
			      <div class="col-md-7">
		  	        <h1><a href="/node/<?php echo $nid ?>"><?php echo $highlight->title ?></a></h1>
		  	        <div class="date">
            	    <?php
                                $date = new DateTime($highlight->field_datum['und'][0]['value']);
                                if ($language->language == 'fr'){
                                    setlocale(LC_ALL, 'fr_CH.UTF8');
                                    $date = strftime('%d %B %Y', $date->getTimestamp());
                                } else {
                                    setlocale(LC_ALL, 'de_CH.utf8');
                                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                                }                
                                echo $date;
            	    ?>
            		</div>
			        <p><span class="teaser">
			          <?php echo $highlight->field_anriss_text_teaser['und'][0]['value']; ?>
			        </span></p>
		        	<p class="text">
	            	  <?php echo $highlight->body[$language->language][0]['summary']; ?>
    	      		</p>
			      	
			      </div>
			      
			      <div class="col-md-5">
			        <img src="/sites/default/files/sbv/news/bilder/<?php echo $highlight->field_bild['und'][0]['filename']; ?>" alt="<?php echo $highlight->field_bild['und'][0]['alt']; ?>" title="<?php echo $highlight->field_bild['und'][0]['title']; ?>"/>
			      </div>
			      
			    </div>
			  </div>
        <?php
        break;
          }
    	}
	   ?>
	</div>



<!-- NEWS out of Drupal Backend -->
<div class="container news-list" role="main">
  	  

<!-- Aktuelles -->
	 <div class="col col-md-6 col-sm-12">
	    <h2 class="header">
           <?php if($language->language == 'fr') { ?>
             Actualités
          <?php } else { ?>
             Aktuell
          <?php } ?>
     	</h2>
   </div>
	 <div class="call-to-actions col-md-6 col-sm-12">
      <div class="logos donate">		        
			        
            <?php if($language->language == 'fr') { ?>
               <a class="btn btn-primary btn-lg" href="/dons" role="button"><?php print t('Faire un don') ?> » <br /> <i class="fa fa-gift" style="display:none;" aria-hidden="true"></i> </a>
            <?php } else { ?>
               <a class="btn btn-primary btn-lg" href="/spenden" role="button"><?php print t('Spenden') ?> » <br /> <i class="fa fa-gift" style="display:none;" aria-hidden="true"></i> </a>
            <?php } ?>
            
            <a href="https://www.zewo.ch/" target="_blank"><img alt="Zewo Logo" src="/sites/all/themes/sbvfsa/front/img/logos/Zewo-Guetesiegel_4c_web.jpg"></a>
              <a href="http://www.swisslabel.ch/" target="_blank"><img alt="Swisslabel Logo" src="/sites/all/themes/sbvfsa/front/img/logos/Swisslabel_logo_web.jpg"></a>
			        <a href="http://www.berufsbildungplus.ch/" target="_blank"><img alt="Berufsbildung plus Logo" src="/sites/all/themes/sbvfsa/front/img/logos/SBFI_Vignette_140mm_DE_web.jpg"></a>
	    </div>
	</div>
	

	
	
     <div class="news col-md-8">
      <div class="row">
      <?php
		$query = new EntityFieldQuery();
		$query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'sbv_news')->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_datum', 'value', 'DESC');
		$result = $query->range(0,4)->execute();
	  ?>   
    <?php
      if (!empty($result['node'])) {
		  $nids = array_keys($result['node']);

		  foreach ($nids as $nid) {
		    $news = node_load($nid, NULL, TRUE);
        $field_language = field_language('node', $news, 'field_date');
       
        // TODO Live - Correct TID for Highlight!
        // Only print News which are NO Highlight!
       if (39 == $news->field_puplikationsoptionen['und'][0]['tid']){
       // We add ScreenReader Only Tag to Highlight Article
        ?>
		   <div class="news col-md-12 ScreenReader">
		   <?php  } else { ?>
		   <div class="news col-md-12">
		   <?php } ?>
		   
         <a href="/node/<?php echo $nid ?>"><h2 class="title"><?php echo $news->title; ?></h2></a>
         <div class="date">
         <?php
                $date = new DateTime($news->field_datum['und'][0]['value']);
                if ($language->language == 'fr'){
                    setlocale(LC_ALL, 'fr_CH.UTF8');
                    $date = strftime('%d %B %Y', $date->getTimestamp());
                } else {
                    setlocale(LC_ALL, 'de_CH.utf8');
                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                }                
                echo $date;
      	 ?>
         </div>
           <p class="text">
             <?php echo $news->field_anriss_text_teaser['und'][0]['value']; ?>
           </p>
       </div>
		<?php 
		  
		  }
		 }
	  ?>
		

      </div>
      <?php if($language->language == 'fr') { ?>
        <a href="/fr/aktuell"><button type="button" class="btn btn-default"><b><?php print t('Toutes les actualités') ?></b></button></a>
      <?php } else { ?>     
        <a href="/aktuell"><button type="button" class="btn btn-default"><b><?php print t('Alle News anzeigen') ?></b></button></a>
      <?php } ?>
   </div>
 
	
</div>



	
<!-- Events -->
	<div class="container events" style="display:none">
	  <?php print render($page['bottom_content']); ?>
	  
	  <h2 class="header">Veranstaltungen</h2>

	  <div class="row">
	    <div class="event col col-md-4 col-sm-6 col-xs-12">
	      <h3>Event 1</h3>
	      <p>Donec id elit non mi porta gravida at eget metus. Fusce 
dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut 
fermentum massa justo sit amet risus. Etiam porta sem malesuada magna 
mollis euismod. Donec sed odio dui. </p>
	      <p><a class="btn btn-default" href="#" role="button">Detail »</a></p>
	    </div>
	    <div class="event col col-md-4 col-sm-6 col-xs-12">
	      <h3>Event 2</h3>
	      <p>Donec id elit non mi porta gravida at eget metus. Fusce 
dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut 
fermentum massa justo sit amet risus. Etiam porta sem malesuada magna 
mollis euismod. Donec sed odio dui. </p>
	      <p><a class="btn btn-default" href="#" role="button">Detail »</a></p>
	   </div>
	    <div class="event col col-md-4 col-sm-12 col-xs-12">
	      <h3>Event 3</h3>
	      <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis 
in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. 
Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
 ut fermentum massa justo sit amet risus.</p>
	      <p><a class="btn btn-default" href="#" role="button">Detail »</a></p>
	    </div>
	  </div>
</div> <!-- end events -->	

	  
	  
	  
	  
	  
	  
<!-- Footer  MASTER -->
    <div style="clear:both;"></div>
    <div class="container">
        <a href="#top" aria-hidden="true"><?php print t('Zurück nach oben') ?></a>
    </div>
    
    <footer class="container-fluid" id="Footer">
	
	<div class="footermenu container" role="navigation">
          <h2 class="header"><?php print t('Der SBV - schweizweit für Sie da') ?></h2>
	      <div class="row">

	        <div class="col col-md-4 col-sm-4 col-xs-12">
	            <?php print render($page['footer_firstcolumn']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12">
	            <?php print render($page['footer_secondcolumn']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12">
	            <?php print render($page['footer_thirdcolumn']); ?> 
	        </div>

	      </div>
	      
	      <?php if($language->language == 'fr') { ?>
	        <div class="container address">FSA, Rue de Genève 88b, CH-1004 Lausanne - Tél. +41 21 651 60 60 - E-Mail: <a href="mailto:secretariat.romand@sbv-fsa.ch">secretariat.romand@sbv-fsa.ch</a></div>
              <?php } else { ?>
                <div class="container address">SBV, Könizstrasse 23, Postfach, 3001 Bern - Tel. +41 31 390 88 00 - E-Mail: <a href="mailto:info@sbv-fsa.ch">info@sbv-fsa.ch</a></div>
              <?php } ?>
	      
	    </div>

	  </footer>
	  
	  <div style="clear:both;"></div>
	  
	  
      <!-- Weisser Balken zwischen Footer und Login --> 
      <div class="container" style="display:none">
	  	  <a href="#top" aria-hidden="true"><?php print t('Zurück nach oben') ?></a>
      </div>
      <div style="clear:both;"></div>
      <div class="container logos" style="display:none">
	        <div class="row">
	          <div class="col col-md-12 col-sm-12 col-xs-12">
	          <?php print render($page['logo_line']); ?>
            </div>
	        </div>
       </div>
       <!-- END Weisser Balken zwischen Footer und Login --> 

       <div style="clear:both;"></div>


        <div class="container-fluid intern" id="Login">
	        <div class="container">
		      <div class="row">
		        <div class="col col-md-4 col-sm-4 col-xs-12"> 
		          <?php print render($page['intern_first']); ?>
		        </div>
		        <div class="col col-md-4 col-sm-4 col-xs-12"> 
		          <?php print render($page['intern_middle']); ?>
		        </div>
		        <div class="col col-md-4 col-sm-4 col-xs-12"> 
		          <?php print render($page['intern_last']); ?>
		        </div>
		      </div>
		    </div>
		    

		    
	    </div>

		<div class="container-fluid usermenu">
	      <div class="row">
	        <div class="col col-md-4 col-sm-4 col-xs-12">  
	          <?php print render($page['user_first']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12"> 
	          <?php print render($page['user_middle']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12"> 
	          <?php print render($page['user_last']); ?>
	        </div>
	      </div>
	    </div>
     <!-- END Footer  MASTER -->






	</div> <!-- /container -->








 <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>

  





<?php /*print render($page['content']); */
/*echo "<h1>START DEBUG DEBUG DEBUG</h1>";
var_dump(get_defined_vars());
echo "<h1>DEBUG DEBUG DEBUG END</h1>";*/
?>
