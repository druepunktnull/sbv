<?php

/**
 * @file
 * sbvfsa's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system folder.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/sbvfsa.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see sbvfsa_process_page()
 */
?>
<!-- Redirect to source domain -->
<script type="text/javascript">
function GetCanonicalDomainName() {

	var url = jQuery('link[rel=canonical]').attr('href');
	var url_parts = url.split('/');
	var domain_name_parts = url_parts[2].split(':');
	var domain_name = domain_name_parts[0];
	return domain_name;

}

/* Either the domain is correct or the canonical link does not contain dots ... so i suppose its a relative path */
if(document.domain==GetCanonicalDomainName() || GetCanonicalDomainName().indexOf('.') == -1 ){
} else {
	var sourceuri = jQuery('link[rel=canonical]').attr('href').replace("fr/fr/", "fr/");
	var sourceuri = sourceuri.replace("de/de/", "de/");
	window.location.replace(sourceuri);
}
</script>

<?php
// Meta Description

if (isset($node)) {
	$fieldinfo = field_get_items('node', $node, 'field_meta');
} else {
	$fieldinfo = false;
}

if ($fieldinfo) {
  $metadescr = check_plain($fieldinfo[0]['value']);
  $data = array(
    '#tag' => 'meta',
    '#attributes' => array(
       'name' => 'description',
       'content' => $metadescr,
    ),
  );
  drupal_add_html_head($data, 'sbv_meta_description');

} else {
  $metadescr = '';
}
	


?>

<!-- MASTER header2 Copy from Homepage! -->
	<!-- MASTER header2 Copy from here! -->
    <div class="container header2">
      <div class="col col-md-12 tagline">
         <?php print render($page['tagline']); ?>
      </div>
      <div class="col col-md-12 tools">
         
        <div class="language"><?php print render($page['tagline_tools']); ?></div>
        <div class="access">
           <ul class="accesstools">
           <!-- Access Tools -->
           <?php 
            if($language->language == 'fr') {
              echo'<li><a aria-hidden="true" onclick="changeLeftStyle(); return false;" title="affichage loupe" class="element-focusable" href="#"><i class="fa fa-caret-square-o-left"></i></a></li>
              <li><a aria-hidden="true" class="increaseFont element-focusable" onclick="resizeText(1); return false;" href="#" title="agrandir l\'écriture"><i class="fa fa-plus-square"></i></a></li>
              <li><a aria-hidden="true" class="decreaseFont element-focusable" onclick="resizeText(-1); return false;" href="#" title="réduire l\'écriture"><i class="fa fa-minus-square"></i></a></li>
              <li><a aria-hidden="true" onclick="changeStyle(); return false;" title="changer le contraste" class="element-focusable" href="#"><i class="fa fa-adjust"></i></a></li>';

            } else {
              echo'<li><a aria-hidden="true" onclick="changeLeftStyle(); return false;" title="Lupenansicht" class="element-focusable" href="#"><i class="fa fa-caret-square-o-left"></i></a></li>
              <li><a aria-hidden="true" class="increaseFont element-focusable" onclick="resizeText(1); return false;" href="#" title="Schrift vergrössern"><i class="fa fa-plus-square"></i></a></li>
              <li><a aria-hidden="true" class="decreaseFont element-focusable" onclick="resizeText(-1); return false;" href="#" title="Schrift verkleinern"><i class="fa fa-minus-square"></i></a></li>
              <li><a aria-hidden="true" onclick="changeStyle(); return false;" title="Kontrast ändern" class="element-focusable" href="#"><i class="fa fa-adjust"></i></a></li>';
            } 
           ?>
         </ul>
        </div>

        <div role="search" class="search"><i class="fa fa-search"></i><?php print render($page['search']); ?></div>        
      </div>
    </div>
	<!-- End MASTER header2 Copy from here! -->

  <!-- End MASTER header2 Copy from homepage! -->



	
  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>



  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix container">

    <?php if ($breadcrumb): ?>
        <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

    <div  class="column container">
      
    <div class="col-md-3" role="navigation">
	      <?php print render($page['sidebar_first']); ?>
	  </div>
	  
	  <div class="section col-md-9" id="content" role="content"><!-- Correct? -->
   
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 name="top" class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>

	<!-- READ Speak Controls -->
	  <?php 
	if(useReadSpeakLang()){
	  if(useReadSpeakLang() == 'fr') {
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=fr_fr&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Ecoutez"> <span class="readspeakerbutton fr"></span></a> </div> <div id=\'xp1\'></div> ';	
		
	  } else if(useReadSpeakLang() == 'de'){
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=de_de&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Vorlesen"> <span class="readspeakerbutton de"></span></a> </div> <div id=\'xp1\'></div> ';
	  } 
	}
	?>

	<div id="readoutloud" class="">
	  <div id="webformerror"></div>
	      <?php print render($page['content']); ?>
	</div>
	      <?php print $feed_icons; ?>
	
    </div></div> <!-- /.section, /#content -->

    <?php if (false && $page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>

  </div></div> <!-- /#main, /#main-wrapper -->

 
 <!-- Footer  Copy from homepage -->
<!-- Footer  MASTER -->
    <div style="clear:both;"></div>
    <div class="container">
        <a href="#top" aria-hidden="true"><?php print t('Zurück nach oben') ?></a>
    </div>
    
    <footer class="container-fluid" id="Footer">
	
	<div class="footermenu container" role="navigation">
          <h2 class="header"><?php print t('Der SBV - schweizweit für Sie da') ?></h2>
	      <div class="row">

	        <div class="col col-md-4 col-sm-4 col-xs-12">
	            <?php print render($page['footer_firstcolumn']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12">
	            <?php print render($page['footer_secondcolumn']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12">
	            <?php print render($page['footer_thirdcolumn']); ?> 
	        </div>

	      </div>
	      
	      <?php if($language->language == 'fr') { ?>
	        <div class="container address">FSA, Rue de Genève 88b, CH-1004 Lausanne - Tél. +41 21 651 60 60 - E-Mail: <a href="mailto:secretariat.romand@sbv-fsa.ch">secretariat.romand@sbv-fsa.ch</a></div>
              <?php } else { ?>
                <div class="container address">SBV, Könizstrasse 23, Postfach, 3001 Bern - Tel. +41 31 390 88 00 - E-Mail: <a href="mailto:info@sbv-fsa.ch">info@sbv-fsa.ch</a></div>
              <?php } ?>
	      
	    </div>

	  </footer>
	  
	  <div style="clear:both;"></div>
	  
	  
      <!-- Weisser Balken zwischen Footer und Login --> 
      <div class="container" style="display:none">
	  	  <a href="#top" aria-hidden="true"><?php print t('Zurück nach oben') ?></a>
      </div>
      <div style="clear:both;"></div>
      <div class="container logos" style="display:none">
	        <div class="row">
	          <div class="col col-md-12 col-sm-12 col-xs-12">
	          <?php print render($page['logo_line']); ?>
            </div>
	        </div>
       </div>
       <!-- END Weisser Balken zwischen Footer und Login --> 

       <div style="clear:both;"></div>


        <div class="container-fluid intern" id="Login">
	        <div class="container">
		      <div class="row">
		        <div class="col col-md-4 col-sm-4 col-xs-12"> 
		          <?php print render($page['intern_first']); ?>
		        </div>
		        <div class="col col-md-4 col-sm-4 col-xs-12"> 
		          <?php print render($page['intern_middle']); ?>
		        </div>
		        <div class="col col-md-4 col-sm-4 col-xs-12"> 
		          <?php print render($page['intern_last']); ?>
		        </div>
		      </div>
		    </div>
		    

		    
	    </div>

		<div class="container-fluid usermenu">
	      <div class="row">
	        <div class="col col-md-4 col-sm-4 col-xs-12">  
	          <?php print render($page['user_first']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12"> 
	          <?php print render($page['user_middle']); ?>
	        </div>
	        <div class="col col-md-4 col-sm-4 col-xs-12"> 
	          <?php print render($page['user_last']); ?>
	        </div>
	      </div>
	    </div>
     <!-- END Footer  MASTER -->
     <!-- END Footer  Copy from Homepage -->

