<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?>
<?php global $language ?>
<!DOCTYPE html>
<html lang="<?php $language->language ?>">

  <head profile="<?php print $grddl_profile; ?>"> 
    <?php print $head; ?>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>
      <?php 
      if($language->language == 'fr'){
        //print $head_title_array['title'] .' | FSA';
        print 'FSA / '. $head_title_array['title'];
      } else {
        //print $head_title;
        print 'SBV / '. $head_title_array['title'];
      } 
      ?>
    </title>

    <meta name="theme-color" content="#488ae7">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/sites/all/themes/sbvfsa/img/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="<?php print base_path() . path_to_theme();?>/front/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php print base_path() . path_to_theme();?>/front/css/sbv.css" rel="stylesheet">
    <link href="<?php print base_path() . path_to_theme();?>/front/css/document_icons.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php print base_path() . path_to_theme();?>/front/css/font-awesome.min.css">

      <?php print $styles; ?>
    <link rel="alternate stylesheet" title="High Contrast" href="<?php print base_path() . path_to_theme();?>/css/negativ.css" media="screen"></link>
    <link rel="alternate stylesheet" title="Left" href="<?php print base_path() . path_to_theme();?>/css/left.css" media="screen"></link>
    <link rel="alternate stylesheet" title="Contrast Left" href="<?php print base_path() . path_to_theme();?>/css/negativLeft.css" media="screen"></link>



    <!-- Youtube Accessibility -->
    <link href="<?php print base_path() . path_to_theme();?>/thirdparty/ytp/css/ytp.css" type="text/css" rel="stylesheet"></link>

 
    
    <script type="text/javascript" src="http://www.sbv-fsa.ch/misc/jquery.js?v=1.4.4"></script>
    <!-- Monkee.ch Custom Accessibility Scripts -->
    <script src="<?php print base_path() . path_to_theme();?>/js/styleswitcher.js" type="text/javascript"></script>
    <script src="<?php print base_path() . path_to_theme();?>/js/monkee.js" type="text/javascript"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>


  <body class="<?php print $classes; ?> <?php print $language->language; ?>" <?php print $attributes;?> name="top">

  <!-- Sprung Links --> 
    <div id="skip-link">
      <a href="#Menu" class="element-invisible element-focusable" accesskey="1"><?php print t('Menü') ?></a>
      <a href="#search-block-form" class="element-invisible element-focusable" accesskey="2"><?php print t('Suche') ?></a>
      <a href="#Content" class="element-invisible element-focusable" accesskey="5"><?php print t('Inhalt') ?></a>
      <a href="#Footer" class="element-invisible element-focusable" accesskey="7"><?php print t('Direktlinks') ?></a>
      <a href="#Login" class="element-invisible element-focusable" accesskey="9"><?php print t('Login') ?></a>

    </div>
  <!-- ENDE Sprung Links-->

    <div class="" role="banner">
      <div class="container">
        <div class="navbar-header col-md-7 col-xs-12">
          <a class="" href="/">
            <?php if($language->language == 'fr') { ?>
               <div class="toplogo"><img src="/sites/all/themes/sbvfsa/front/img/sbv_logo_100_fr.jpg"></img></div>
            <?php } else { ?>
               <div class="toplogo"><img src="/sites/all/themes/sbvfsa/front/img/sbv_logo_100_de.jpg"></img></div>
            <?php } ?>
            
          </a>
        </div>
       <div class="contact col-md-5 col-xs-12">
            <?php if($language->language == 'fr') { ?>
               <span>Tél. +41 21 651 60 60</span>
            <?php } else { ?>
               <span>Tel. +41 31 390 88 00</span>
            <?php } ?>
       </div>
     </div>
    </div>

    <?php print $page; ?>
    
    <!-- Top Link --> 
    <div class="container">
		<a href="#top" aria-hidden="true"><?php print t('Zurück nach oben') ?></a>
    </div>





  <?php print $scripts; ?>
  <?php print $page_bottom; ?>


  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <!--<script src="<?php print base_path() . path_to_theme();?>/front/js/jquery.js"></script>-->
  <script src="<?php print base_path() . path_to_theme();?>/front/js/bootstrap.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="<?php print base_path() . path_to_theme();?>/front/js/ie10-viewport-bug-workaround.js"></script>
    
    <!-- Youtube Accessibility -->
    <script src="<?php print base_path() . path_to_theme();?>/thirdparty/ytp/js/ytp.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
    <?php
    if($language->language == 'fr') {
              echo '<script src="http://f1.eu.readspeaker.com/script/6122/rs_embhl_v2_de_de.js" type="text/javascript"></script>';   
        echo '<script src="http://f1.eu.readspeaker.com/script/6122/rs_embhl_v2_fr_fr.js" type="text/javascript"></script>';
    } else {
              echo '<script src="http://f1.eu.readspeaker.com/script/6122/rs_embhl_v2_fr_fr.js" type="text/javascript"></script>';
        echo '<script src="http://f1.eu.readspeaker.com/script/6122/rs_embhl_v2_de_de.js" type="text/javascript"></script>';  
    } 
    ?>
	

    <!-- WebForms Accessibility Fix -->
    <script type="text/javascript">
      jQuery('input.required').attr('aria-required',true);
    </script>
    
       <?php print $page_top; ?>  




  </body>
</html>
