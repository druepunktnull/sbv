<?php

/**
 *
 * monkee.ch
 *
 *** Single Course Template ***
 * @author manfred@monkee.ch
 *
 */
?>


<?php include 'monkee.header.master.php'; ?>

    
  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>






<div class="container veranstaltung-list news-list" role="main">
<!-- Aktuelles -->

    <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

  <div class="row">
	 
      <div class="col-md-3" role="navigation">
		  <?php print render($page['sidebar_first']); ?>
      </div>
    <div class="news col-md-9" id="content">
		
	  <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
		
    	<h1 class="title"><?php echo $node->title; ?> </h1>

     	<p>
     	
     	  </p>
     	  
     	  
     	 
     	  
     	  
     	  
     	  <h2><?php t("Datum der veranstaltung"); ?></h2>
     	  
     	  <span class="date_fine">
     	  <?php

            $date = new DateTime($node->field_datum_event['und'][0]['value']);
						#$date->setTimezone(new DateTimeZone('Europe/Zurich'));
						date_default_timezone_set('Europe/Zurich');
            if ($language->language == 'fr'){
                setlocale(LC_ALL, 'fr_CH.UTF8');
                $date = strftime('%d %B %Y %H:%M', $date->getTimestamp() + 7200);
            } else {
                setlocale(LC_ALL, 'de_CH.utf8');
                $date = strftime('%d. %B %Y %H:%M', $date->getTimestamp()) + 7200;
            }                
            echo $date;
            
            if ($node->field_datum_event['und'][0]['value2']) {
                $date2 = new DateTime($node->field_datum_event['und'][0]['value2']);
                 if ($language->language == 'fr'){
                    setlocale(LC_ALL, 'fr_CH.UTF8');
                    $date2 = strftime('%d %B %Y %H:%M', $date2->getTimestamp() + 7200);
                } else {
                    setlocale(LC_ALL, 'de_CH.utf8');
                    $date2 = strftime('%d. %B %Y %H:%M', $date2->getTimestamp() + 7200);
                }
            if ($date != $date2) {
              echo " - " . $date2;
            }           
            
            }
  
     	  ?>
     	</span>

     	</p>

     	
     	<p>
        <img class="main" src="<?php echo file_create_url($node->field_bild['und'][0]['uri']); ?>" alt="<?php echo $node->field_bild['und'][0]['alt']; ?>"/>
        
      </p>
     	
      <p>
        <span class="teaser"><b>
          <?php if ($node->field_untertitel['und']) { ?>
            <p><?php print $node->field_untertitel['und'][0]['value']; ?></p>
          <?php } ?>
        </b></span>
      </p>
      <p class="text">
        <?php echo $node->body[$language->language][0]['value']; ?>
      </p>
      
      
      
      <?php if ($node->field_ort['und']) { ?>
        <h2><?php print t('Ort') ?></h2>
        <p><?php print $node->field_ort['und'][0]['value']; ?></p>
      <?php } ?>
      
      <?php if ($node->field_adresse['und']) { ?>
        <h2><?php print t('Adresse') ?></h2>
        <p><?php print $node->field_adresse['und'][0]['value']; ?></p>
      <?php } ?>
      
       <?php if ($node->field_preis['und']) { ?>
        <h2><?php print t('Preis') ?></h2>
        <p><?php print $node->field_preis['und'][0]['value']; ?></p>
       <?php } ?>
       
       <?php if ($node->field_reservation['und']) { ?>
        <h2><?php print t('Reservation') ?></h2>
        <p><?php print $node->field_reservation['und'][0]['value']; ?></p>
       <?php } ?>
      
       <?php if ($node->field_telefon['und']) { ?>
        <h2><?php print t('Telefonnummer') ?></h2>
        <p><?php print $node->field_telefon['und'][0]['value']; ?></p>
       <?php } ?>
       
       <?php if ($node->field_email['und']) { ?>
        <h2><?php print t('Email') ?></h2>
        <p><a href="mailto:<?php print $node->field_email['und'][0]['value']; ?>"><?php print $node->field_email['und'][0]['value']; ?></a></p>
       <?php } ?>
      
      
       <?php if ($node->field_spezielles['und']) { ?>
        <h2><?php print t('Spezielles') ?></h2>
        <p><?php print $node->field_spezielles['und'][0]['value']; ?></p>
       <?php } ?> 
      
      
      
      
      

      <div class="assets">
		  <?php if ($node->field_downloads['und']) { ?>
        <h2><?php print t('Downloads') ?></h2>
          <ul>
          <?php 
            foreach ($node->field_downloads['und'] as $download) {
              echo '<li><a href="';
              echo file_create_url($download['uri']);
              echo '">';
              echo $download['description'];
              echo '</a></li>';
            }
          ?>
          </ul>
          <?php } ?>

        <?php if ($node->field_links['und']) { ?>
        <h2><?php print t('Links') ?></h2>
        <ul>
          <?php 
            foreach ($node->field_links['und'] as $links) {
              echo '<li><a href="';
              echo $links['url'];
              echo '">';
              echo $links['title'];
              echo '</a></li>';
            }
          ?>
          </ul>
          <?php } ?>
      </div> <!-- assets -->
      
     
    </div>
    
  </div>
  
</div>


 <?php include 'monkee.footer.master.php'; ?>



	  
	  
	  
	  
	  
	  





