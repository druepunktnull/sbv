<?php

/**
 * @file
 * sbvfsa's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system folder.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or /themes//sbvfsa.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see sbvfsa_process_page()
 */
?>


<?php include 'monkee.header.master.php'; ?>

    
  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>






<div class="container news-detail" role="main">
<!-- Aktuelles -->

    <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

  <div class="row">
	 
      <div class="col-md-3" role="navigation">
		  <?php print render($page['sidebar_first']); ?>
      </div>
      
      



    <div class="news col-md-9" id="content">
		
  <?php if ($tabs): ?>
    <div class="tabs">
      <?php print render($tabs); ?>
    </div>
  <?php endif; ?>

		
    	<h1 class="page-title"><?php echo $node->title; ?> </h1>
    	
    		<!-- READ Speak Controls -->
	  <?php 
	if(useReadSpeakLang()){
	  if(useReadSpeakLang() == 'fr') {
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=fr_fr&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Ecoutez"> <span class=" fr"><i class="fa fa-headphones" aria-hidden="true"></i> Ecoutez</span></a> </div> <div id=\'xp1\'></div> ';	
		
	  } else if(useReadSpeakLang() == 'de'){
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=de_de&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Vorlesen"> <span class=" de"><i class="fa fa-headphones" aria-hidden="true"></i> Vorlesen</span></a> </div> <div id=\'xp1\'></div> ';
	  } 
	}
	?>

 			<div id="readoutloud">
     	  <span class="date_fine">
     	  <?php
                $date = new DateTime($node->field_datum['und'][0]['value']);
                if ($language->language == 'fr'){
                    setlocale(LC_ALL, 'fr_CH.UTF8');
                    $date = strftime('%d %B %Y', $date->getTimestamp());
                } else {
                    setlocale(LC_ALL, 'de_CH.utf8');
                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                }                
                echo $date;
     	  ?>
     	</span>
     	 <?php 
     	  if ($node->field_puplikationsoptionen['und'][0]['tid'] == 40){
     	  // 40 ist Medienmitteilung
         	if ($language->language == 'fr'){
                echo '<span class="badge">Communiqué de presse</span>';
            } else {
                echo '<span class="badge">Medienmitteilung</span>';
            }            
          }
        ?>
     	</p>
      <p>
	<?php if($node->field_bild): ?>
        <img src="<?php echo file_create_url($node->field_bild['und'][0]['uri']); ?>" alt="<?php echo $node->field_bild['und'][0]['alt']; ?>"/>
        <?php endif; ?>
      </p>
      <p>
        <span class="teaser"><b>
          <?php echo $node->field_anriss_text_teaser['und'][0]['value']; ?>
        </span></b>
      </p>
      <p class="text">
        <?php echo $node->body[$language->language][0]['value']; ?>
      </p>

      <div class="assets">
		  <?php if ($node->field_downloads['und']) { ?>
        <h2><?php print t('Downloads') ?></h2>
          <ul>
          <?php 
            foreach ($node->field_downloads['und'] as $download) {
              echo '<li><a href="';
              echo file_create_url($download['uri']);
              echo '">';
              echo $download['description'];
              echo '</a></li>';
            }
          ?>
          </ul>
          <?php } ?>

        <?php if ($node->field_links['und']) { ?>
        <h2><?php print t('Links') ?></h2>
        <ul>
          <?php 
            foreach ($node->field_links['und'] as $links) {
              echo '<li><a href="';
              echo $links['url'];
              echo '">';
              echo $links['title'];
              echo '</a></li>';
            }
          ?>
          </ul>
          <?php } ?>
      </div> <!-- assets -->
      
      <?php if($language->language == 'fr') { ?>
        <a href="/actualites"><button type="button" class="btn btn-default"><b><?php print t('Toutes les actualités') ?></b></button></a>
      <?php } else { ?>     
        <a href="/aktuell"><button type="button" class="btn btn-default"><b><?php print t('Alle News anzeigen') ?></b></button></a>
      <?php } ?>
    </div>
    
  </div>
  

  
</div>
</div>


 <?php include 'monkee.footer.master.php'; ?>



	  
	  
	  
	  
	  
	  





