<!-- HEADER -->
<?php include 'monkee.header.master.php'; ?>



<?php 
  /* Initialize Variables */
  $list_start = 0;
  $list_range = 15;
  if ($_GET["start"]){
    $list_start = $_GET["start"];
  }
  if ($_GET["range"]){
    $list_range = $_GET["range"];
  }
?>




	
  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, #messages -->
  <?php endif; ?>



  <div id="main-wrapper container" class="clearfix"><div id="main" class="clearfix container">

    <?php if ($breadcrumb): ?>
        <div id="breadcrumb" class="col-md-12"><?php print $breadcrumb; ?></div>
    <?php endif; ?>
      
<div class="col-md-3 main-navigation" role="navigation">
	      <?php print render($page['sidebar_first']); ?>
	      
    <div class="call-to-actions">
      <div class="logos donate col-md-12" style="display:none;">
        
       <div class="col-md-12">
          <?php if($language->language == 'fr') { ?>
            <h3><a href="/dons" aria-hidden="true"><?php print t('Spenden') ?></a></h3>
          <?php } else {?>
            <h3><a href="/spenden" aria-hidden="true"><?php print t('Spenden') ?></a></h3>
          <?php } ?>
       </div>
       <div class="col-md-12"><p><?php print t('Unterstützen Sie') ?> <br /><?php print t('den SBV') ?></p></div>
       
       <?php if($language->language == 'fr') { 
              $alt_zewo = "Vers le site de la Fondation ZEWO - Service suisse de certification pour les organisations d'utilité publique collectant des dons";
              $url_zewo = "https://www.zewo.ch/fr";
              $url_spenden = "/dons_en_ligne";
            } else {
              $alt_zewo = "Zur Website der Stiftung ZEWO - Schweizerische Zertifizierungsstelle für gemeinnützige Spenden sammelnde Organisationen";
              $url_zewo = "https://www.zewo.ch/"; 
              $url_spenden = "/spenden";
            } 
           ?>

     <div class="col-md-12">
         <p><a href="<?php echo $url_spenden; ?>" role="button"><i class="fa fa-heart"></i> <?php print t('Jetzt online spenden') ?></a></p>
     </div>
     
     <div class="col-md-12">
        <a href="<?php echo $url_zewo; ?>" title="<?php echo $alt_zewo; ?>" ><img src="/sites/all/themes/monkeeterminal8/logos/logo-zewo-2x.png"/></a>
     </div>
            
                           
   </div>
</div>
</div>	  
	  
	  
	  
	  
	  
	  
<div class="section col-md-9" id="content" role="content"><!-- Correct? -->
  
  <?php if ($tabs): ?>
     <div class="tabs">
       <?php print render($tabs); ?>
     </div>
   <?php endif; ?>
	
	 
      <?php print render($title_prefix); ?>
      <?php if ($title): ?></i>
        <h1 name="top" class="title" id="page-title">
          <?php print $title; ?>
        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      
      
    
      
     

	<!-- READ Speak Controls -->
	  <?php 
	if(useReadSpeakLang()){
	  if(useReadSpeakLang() == 'fr') {
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=fr_fr&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Ecoutez"> <span class="readspeakerbutton fr"></span></a> </div> <div id=\'xp1\'></div> ';	
		
	  } else if(useReadSpeakLang() == 'de'){
				echo '<div id="readspeaker_button1" class="rs_skip"> <a accesskey="L" href="http://app.eu.readspeaker.com/cgi-bin/rsent?customerid=6122&amp;lang=de_de&amp;readid=readoutloud&amp;url='. selfURL() .'" onclick="readpage(this.href, \'xp1\'); return false;" title="Vorlesen"> <span class="readspeakerbutton de"></span></a> </div> <div id=\'xp1\'></div> ';
	  } 
	}
	?>

<div id="readoutloud" class="">
	  <div id="webformerror"></div>
	    <?php print render($page['help']); ?>
	    
	    <?php if ($node->field_bild['und'][0]) { ?>
          <img src="<?php echo file_create_url($node->field_bild['und'][0]['uri']); ?>" alt="<?php echo $node->field_bild['und'][0]['alt']; ?>"/>
        <?php } ?>
	    
	    
	    <?php print render($page['content']); ?>
	      
	      
	      


              <!-- NEWS out of Drupal Backend -->
              <div class="news-list category" role="main">
              <!-- Aktuelles -->
                 <div class="news">


                    
                    
               <?php
		   
		              $query = new EntityFieldQuery();
		              
		              // Kategorie? Query
		              if ($kategorie = $node->field_kategorie['und'][0]['tid']){
                     $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'sbv_news')->fieldCondition('field_kategorie', 'tid', $kategorie)->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_datum', 'value', 'DESC');
                  } else {
		                $query->entityCondition('entity_type', 'node')->entityCondition('bundle', 'sbv_news')->propertyCondition('language', $language->language, '=')->fieldOrderBy('field_datum', 'value', 'DESC');
		              }
		              
		              $result = $query->range($list_start,$list_range)->execute();
	             ?>   
           
              <?php
                if (!empty($result['node'])) {
                $nids = array_keys($result['node']);

                  foreach ($nids as $nid) {
                      $news = node_load($nid, NULL, TRUE);
                      $field_language = field_language('node', $news, 'field_date');
                  ?>
                  
                      
                      
                    <!-- Highlight? One News Line -->
                    <?php
                        if($language->language == 'fr') {
                          $highlight_text = "À la une";
                        } else {
                          $highlight_text = "Highlight";
                        }
                        if ($news->field_puplikationsoptionen['und'][0]['tid'] == 39){
                            echo '<div class="news highlight col-md-12">';
                            echo '<div class="highlightbatch"><i class="fa fa-star"></i> '. $highlight_text .'</div><div class="clear"></div>';
                        } else {
                            echo '<div class="news col-md-12">';
                        }       
                    ?>  

                       <div class="date">
                        <?php
                                $date = new DateTime($news->field_datum['und'][0]['value']);
                                if ($language->language == 'fr'){
                                    setlocale(LC_ALL, 'fr_CH.UTF8');
                                    $date = strftime('%d %B %Y', $date->getTimestamp());
                                } else {
                                    setlocale(LC_ALL, 'de_CH.utf8');
                                    $date = strftime('%d. %B %Y', $date->getTimestamp());
                                }                
                                echo $date;
                      	 ?>
                        	<?php 
       	                  if ($news->field_puplikationsoptionen['und'][0]['tid'] == 40){
                       	    // 40 ist Medienmitteilung
                            echo '<span class="badge">Medienmitteilung</span>';
                          }
                          ?>
                       </div>
                         
                       <a href="/node/<?php echo $nid ?>"><h2 class="title"><?php echo $news->title; ?></h2></a>
                         
                       <p class="text">

                         <?php echo $news->field_anriss_text_teaser['und'][0]['value']; ?>
                       </p>
                     </div> <!-- END One News Line -->
		              <?php 
		                
		                }
		               }
	                ?>
		

                    </div>

                  
                    <?php print $feed_icons; ?>
               
                </div>
	      
	      
	      
	      
	      
	  </div><!-- readOutLoud -->
	     
<?php
  // Pagination
  if ($list_start > 0){
    echo '<a href="?start=0&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Erste Seite") .' (1 - '.$list_range.')</button></a>';
  }
  if ($list_start > 0){
    $sum = $list_start - $list_range;
    $previous_start = 0;
    if ($sum > 0){
      $previous_start = $sum;
      echo '<a href="?start='. $previous_start .'&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Vorherige Seite") .' ('.$previous_start.' - '.$list_start.')</button></a>';
    }
  }
  $next_start = $list_start + $list_range;
  $next_end = $next_start + $list_range;
  echo '<a href="?start='. $next_start .'&range='.$list_range.'"><button class="btn btn-default" type="button">'. t("Nächste Seite") .' ('.$next_start.' - '.$next_end.')</button></a>'
?>	      
	  
	      
	      
</div></div> <!-- /.section, /#content -->







    <?php if (false && $page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>

  </div></div> <!-- /#main, /#main-wrapper -->









<!-- ADMIN STUFF -->
 

  
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>
	

	




  <?php include 'monkee.footer.master.php'; ?>

</div> <!-- /container -->


<!-- ADMIN Stuff -->
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>

  





<?php /*print render($page['content']); */
/*echo "<h1>START DEBUG DEBUG DEBUG</h1>";
var_dump(get_defined_vars());
echo "<h1>DEBUG DEBUG DEBUG END</h1>";*/
?>
