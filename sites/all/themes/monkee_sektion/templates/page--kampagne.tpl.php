<?php

/**
 * @file
 * sbvfsa's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system folder.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or /themes//sbvfsa.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see sbvfsa_process_page()
 */
?>
<!--<html>
<head>

    <title>SBV Kampagne</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="generator" content="monkee - http://monkee.ch">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
-->
    <!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
  <!--
    <link rel="shortcut icon" href="http://www.sbv-fsa.ch/themes/sbvfsa/img/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/sbvfsa/kampagne/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/sbvfsa/kampagne/css/layout.css">
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/sbvfsa/kampagne/css/typography.css">
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/sbvfsa/kampagne/css/form.css">

<!--
</head>
<body>
-->
<?php
// Meta Description

if (isset($node)) {
	$fieldinfo = field_get_items('node', $node, 'field_meta');
} else {
	$fieldinfo = false;
}
if ($fieldinfo) {
  $metadescr = check_plain($fieldinfo[0]['value']);
}
else {
  $metadescr = 'Der Schweizerische Blinden- und Sehbehindertenverband ist die nationale Selbsthilfeorganisation mit regionalen Beratungs- und Bildungsangeboten in allen Landesteilen.';
}
	
$data = array(
    '#tag' => 'meta',
    '#attributes' => array(
       'name' => 'description',
       'content' => $metadescr,
    ),
);
drupal_add_html_head($data, 'sbv_meta_description');

?>


<!-- Kampagne CSS -->
    <link rel="shortcut icon" href="http://www.sbv-fsa.ch/sites/all/themes/sbvfsa/img/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/sbvfsa/kampagne/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/sbvfsa/kampagne/css/layout.css">
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/sbvfsa/kampagne/css/typography.css">
    <link rel="stylesheet" type="text/css" href="/sites/all/themes/sbvfsa/kampagne/css/form.css">



<!--  SBV SBV SBV -->
<!--  SBV SBV SBV -->
<!--  SBV SBV SBV -->



		     <?php 
			if($language->language == 'fr') {
	    		echo'<div class="ScreenReader">Liebe Nutzer von Jaws und Braillezeile, willkommen auf der Seite zur neuen Sensibilisierungskampagne des Schweizerischen Blinden- und Sehbehindertenverbands. Die Kampagne richtet sich sowohl an die breite Bevölkerung als auch an betroffene Menschen und ihr Umfeld. Um auf die normale Homepage des SBV zu springen, benutzen Sie den Link "zur SBV Hauptseite".</div>';
			} else {
			    echo'<div class="ScreenReader">Liebe Nutzer von Jaws und Braillezeile, willkommen auf der Seite zur neuen Sensibilisierungskampagne des Schweizerischen Blinden- und Sehbehindertenverbands. Die Kampagne richtet sich sowohl an die breite Bevölkerung als auch an betroffene Menschen und ihr Umfeld. Um auf die normale Homepage des SBV zu springen, benutzen Sie den Link "zur SBV Hauptseite".</div>';		 
			} ?>




  <div class="header">

    <div class="inner">
	  <!--<p class="brand">Sensibilisierungskampagne</p>-->
	  <!--<img src="/sites/all/themes//sbvfsa/kampagne/img/menubar.png" alt="Logo" width="80%" style="float:left;position:absolute;top:0px;left:0px;"/>-->
	    <div class="logo">
	      <span class="nav-open-button">
			<img id="burgerbutton" src="/sites/all/themes/sbvfsa/kampagne/img/burger.png" alt=""/>
		  </span>         
		     <?php 
			if($language->language == 'fr') {
	    		    echo'<img src="/sites/all/themes/sbvfsa/kampagne/img/logo_fr.png" alt=""/>';
			} else {
			    echo'<img src="/sites/all/themes/sbvfsa/kampagne/img/logo.png" alt=""/>';		 
			} ?>


	         <div class="access" id="accesstools">
				<ul> <!-- float:right, last ist first on the left ;-) -->
					<li>
 					  <span class="language">
					    <a href="/de/node/2395" class="de" title="Seite auf Deutsch">DE</a> | 
					    <a href="/fr/node/2396" class="fr" title="Site en francais">FR</a>
				      </span>
					</li>
	  <?php 
		if($language->language == 'fr') {
	    	echo'<li><a aria-hidden="true" class="decreaseFont element-focusable" onclick="resizeText(-1); return false;" href="#" title="Schrift verkleinern"><span class="minus"></span></a></li>
			 <li><a aria-hidden="true" class="increaseFont element-focusable" onclick="resizeText(1); return false;" href="#" title="Schrift vergrössern"><span class="plus"></span></a></li>
			 <li><a aria-hidden="true" onclick="changeLeftStyle(); return false;" title="layout alternatif" class="element-focusable" href="#"><span class="goleft"></span></a></li>
			 <li><a aria-hidden="true" onclick="changeStyle(); return false;" title="changer Kontrast" class="element-focusable" href="#"><span class="contrast"></span></a></li>';

	    } else {
 			echo'<li><a aria-hidden="true" class="decreaseFont element-focusable" onclick="resizeText(-1); return false;" href="#" title="Schrift verkleinern"><span class="minus"></span></a></li>
			 <li><a aria-hidden="true" class="increaseFont element-focusable" onclick="resizeText(1); return false;" href="#" title="Schrift vergrössern"><span class="plus"></span></a></li>
			 <li><a aria-hidden="true" onclick="changeLeftStyle(); return false;" title="Linksbündig" class="element-focusable" href="#"><span class="goleft"></span></a></li>
			 <li><a aria-hidden="true" onclick="changeStyle(); return false;" title="Kontrast ändern" class="element-focusable" href="#"><span class="contrast"></span></a></li>';
	  } 
	?>
				
				</ul>
				
			</div>
			
	    </div>

	  <div class="navigation primary">
	    <ul style="display: block;" id="menu" role="navigation">
	      <?php 
			if($language->language == 'fr') { 
 			  echo '<li class="link"><a href="#sehbehinderung" title="Handicap visuel">Handicap visuel</a></li>			  
			        <li class="link"><a href="#sbvhilft" title="Comment aide la FSA">Comment aide la FSA</a></li>
			        <li class="link"><a href="#plakate" title="Campagne">Campagne</a></li>		  
			        <li class="link"><a href="#medien" title="Médias">Médias</a></li>
			        <li class="link"><a href="/fr/homefr" title="Site FSA">Site FSA</a></li>'; 
			} else {	
				echo '<li class="link"><a href="#sehbehinderung" title="Sehbehinderung">Sehbehinderung</a></li>			  
			        <li class="link"><a href="#sbvhilft" title="Der SBV hilft">Der SBV hilft</a></li>
			        <li class="link"><a href="#plakate" title="Kampagne">Kampagne</a></li>		  
			        <li class="link"><a href="#medien" title="Medien">Medien</a></li>
			        <li class="link"><a href="/de/home" title="Hauptseite SBV">SBV Hauptseite</a></li>';
			} 
 		 ?>

	  	  <div class="wrapper sbvclear spacer"></div>
	    </ul>
	  </div>



	</div>
  </div>












	  <!-- 
	       ***********************
	       Inhalt
	       ***********************
	    -->
  <div class="main" role="main">
	<div class="content-container typography">	



	  
	  <div id="abca" class="wrapper sbvblue">
	    <div  class="sbvblue  article">
	      <div class="text middle">
		    <?php
			  /* Feld 1 - Titel, normaler Body */
			  $body2 = field_get_items('node',$node, 'body');
			  print $body2[0]['value'];
			?>
	      </div>

	    </div>
	  </div>
	  <div style="clear:both"></div>



	  <div id="kampagne" class="wrapper sbvclear">		
	    <div  class="sbvclear article">
	      <div class="text">
			<?php
  			/* ExtraFeld 1 - Kampagne Info*/
			  $field_inhalt_2 = field_get_items('node',$node, 'field_inhalt_2');
			  print $field_inhalt_2[0]['value'];
			?>
	      </div>
	    </div>
	  </div>
	  <div style="clear:both"></div>
	  
	  
	  

	  <div id="sehbehinderung" class="wrapper sbvblue">
	    <div class="sbvblue Alltag article">
	      <div class="text">
			<?php
			  /* ExtraFeld 2 - DiaShow Alltag */
			  $field_inhalt_2 = field_get_items('node',$node, 'field_inhalt_2');
			  print $field_inhalt_2[1]['value'];
			?>
	      </div>
	    </div>
	  </div>
	  <div style="clear:both"></div>





	  
	  <div id="sbvhilft" class="wrapper sbvclear">
	    <div class="sbvclear article">
	      <div class="text">
			<?php
			  /* ExtraFeld 3 - SBV Hilft Info*/
			  $field_inhalt_2 = field_get_items('node',$node, 'field_inhalt_2');
			  print $field_inhalt_2[2]['value'];
			?>
	      </div>
	    </div>
	  </div>
	  <div style="clear:both"></div>
		
	  <div id="plakate" class="wrapper sbvblue">
	    <div class="sbvblue  article">
	      <div class="text">
			<?php
			  /* ExtraFeld 4 - DiaShow Plakate */
			  $field_inhalt_2 = field_get_items('node',$node, 'field_inhalt_2');
			  print $field_inhalt_2[3]['value'];
			?>
	      </div>
	    </div>
		<div class="video">
 		<?php 
			if($language->language == 'fr') {
	            echo'<iframe src="//www.youtube.com/embed/MrEgAgyT0jA?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
			} else {
			    echo'<iframe src="//www.youtube.com/embed/l-5yTMrFEng?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';	 
			} ?>
		</div>

 		<?php  /*
			if($language->language == 'fr') {
	    		    echo'<div class="ytplayerbox"><span class="ytplayeraspect: wide">&nbsp;</span><span class="ytmovieurl: MrEgAgyT0jA">TV-Spot: Orange?</span></div>';
			} else {
			        echo'<div class="ytplayerbox"><span class="ytplayeraspect: wide">&nbsp;</span><span class="ytmovieurl: l-5yTMrFEng">TV-Spot: Orange?</span></div>';		 
			} */ ?>


	  </div>
	  <div style="clear:both"></div>
	  
	  
	  

	  <div id="medien" class="wrapper sbvclear">		
	    <div class="sbvclear  article">
	      <div class="text">
			<?php
			  /* ExtraFeld 5 - SBV Medien Info*/
			  $field_inhalt_2 = field_get_items('node',$node, 'field_inhalt_2');
			  print $field_inhalt_2[4]['value'];
			?>
	      </div>
	    </div>
	  </div>
	  <div style="clear:both"></div>




	  <!-- 
	       ***********************
	       Ende Inhalt
	       ***********************
	    -->
	</div>
      </div>


      <footer class="footer" role="contentinfo">
	<div class="inner copy">
	  <div class="left"><!--SBV <span class="arrow">→</span> -->
	    <nav class="primary">
	      <ul style="display: block;" id="menu" role="navigation">
	      <?php 
			if($language->language == 'fr') { 
 			  echo '<li class="link"><a href="#sehbehinderung" title="Handicap visuel">Handicap visuel</a></li>			  
			        <li class="link"><a href="#sbvhilft" title="Comment aide la FSA">Comment aide la FSA</a></li>
			        <li class="link"><a href="#kampagne" title="Campagne">Campagne</a></li>		  
			        <li class="link"><a href="#medien" title="Médias">Médias</a></li>
			        <li class="link"><a href="http://www.sbv-fsa.ch/fr/homefr" title="Site FSA">Site FSA</a></li>'; 
			} else {	
				echo '<li class="link"><a href="#sehbehinderung" title="Sehbehinderung">Sehbehinderung</a></li>			  
			        <li class="link"><a href="#sbvhilft" title="Der SBV hilft">Der SBV hilft</a></li>
			        <li class="link"><a href="#kampagne" title="Kampagne">Kampagne</a></li>		  
			        <li class="link"><a href="#medien" title="Medien">Medien</a></li>
			        <li class="link"><a href="http://www.sbv-fsa.ch/de/home" title="Hauptseite SBV">SBV Hauptseite</a></li>';
			} 
 		 ?>
	    </ul>
	    </nav>
	  </div>
	</div>
      </footer>


      <script type="text/javascript" src="/sites/all/themes/sbvfsa/kampagne/js/jquery-1.js"></script>
      <script type="text/javascript" src="/sites/all/themes/sbvfsa/kampagne/js/script.js"></script></div>

<!-- bxSlider -->
<script src="/sites/all/themes/sbvfsa/kampagne/js/jquery.bxslider.monkee.accessible.js"></script>
<link href="/sites/all/themes/sbvfsa/kampagne/css/jquery.bxslider.css" rel="stylesheet" />

<script type="text/javascript">
    

  jQuery(document).ready(function(){
    jQuery('.AlltagSlider').bxSlider({
      pager: false,
      auto: false,
      pause: 6000,
      preventDefaultSwipeY: false,
      preventDefaultSwipeX: false,
      responsive: true,
      slideMargin: 10,
	  infiniteLoop:false,
	  hideControlOnEnd:true
    });

    jQuery('.PlakatSlider').bxSlider({
      pager: false,
      auto: false,
      pause: 6000,
      preventDefaultSwipeY: false,
      responisve: true,
      slideMargin: 10,
	  infiniteLoop:false,
	  hideControlOnEnd:true
    });

	var menu = jQuery('.tablet-nav .header .primary ul');
        menu.addClass("ScreenReader");
    
  });



  jQuery(window).resize(function() {

    jQuery('.AlltagSlider').bxSlider().reloadSlider();
    jQuery('.PlakatSlider').bxSlider().reloadSlider();


  });


</script>




<!--  SBV SBV SBV -->
<!--  SBV SBV SBV -->
<!--  SBV SBV SBV -->

  
    <?php /*print render($page['content']); */
/*echo "<h1>START DEBUG DEBUG DEBUG</h1>";
  var_dump(get_defined_vars());
echo "<h1>DEBUG DEBUG DEBUG END</h1>";*/
?>

	 
     <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>




