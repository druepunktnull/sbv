<?php

/**
 * @file
 * sbvfsa's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system folder.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/sbvfsa.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see sbvfsa_process_page()
 */
?>
<html>
<head>

    <title>SBV Kampagne</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="generator" content="monkee - http://monkee.ch">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">

    <!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
   
    <link rel="shortcut icon" href="http://www.sbv-fsa.ch/themes/sbvfsa/img/favicon.ico">
    <link rel="stylesheet" type="text/css" href="kampagne/css/reset.css">
    <link rel="stylesheet" type="text/css" href="kampagne/css/layout.css">
    <link rel="stylesheet" type="text/css" href="kampagne/css/typography.css">
    <link rel="stylesheet" type="text/css" href="kampagne/css/form.css">

    <!--<link rel="alternate" type="application/rss+xml" title="News" href="http://hottrail.probiermau.ch/race/news/feed">-->

</head>
<body>

<?php
// Meta Description
/*
if (isset($node)) {
	$fieldinfo = field_get_items('node', $node, 'field_meta');
} else {
	$fieldinfo = false;
}
if ($fieldinfo) {
  $metadescr = check_plain($fieldinfo[0]['value']);
}
else {
  $metadescr = 'Der Schweizerische Blinden- und Sehbehindertenverband ist die nationale Selbsthilfeorganisation mit regionalen Beratungs- und Bildungsangeboten in allen Landesteilen.';
}
	
$data = array(
    '#tag' => 'meta',
    '#attributes' => array(
       'name' => 'description',
       'content' => $metadescr,
    ),
);
drupal_add_html_head($data, 'sbv_meta_description');
*/
?>




<!--  SBV SBV SBV -->
<!--  SBV SBV SBV -->
<!--  SBV SBV SBV -->


 <!-- Sprungmarken -->
  <div class="ScreenReader">
    <a class="element-invisible element-focusable" accesskey="1" href="#menu">Menü</a>
	<a class="element-invisible element-focusable" accesskey="7" href="http://www.sbv-fsa.ch/">zur normalen SBV Webseite</a>
	    <!--<a class="element-invisible element-focusable" accesskey="6" href="#accesstools">Darstellung</a>
	    <a class="element-invisible element-focusable" accesskey="7" href="#footer-columns">Schnellzugriff</a>-->
  </div>





  <div class="header">

    <div class="inner">
	  <!--<p class="brand">Sensibilisierungskampagne</p>-->
	  <!--<img src="kampagne/img/menubar.png" alt="Logo" width="80%" style="float:left;position:absolute;top:0px;left:0px;"/>-->
	    <div class="logo">
	      <span class="nav-open-button"><img id="burgerbutton" src="kampagne/img/burger.png" alt="Menü"/></span>         
	      <img src="kampagne/img/logo.png" alt="Logo"/>
	      <img src="kampagne/img/contrast.png" alt="Kontrast"/>
          <img src="kampagne/img/left.png" alt="Linksbündig"/>
          <img src="kampagne/img/plus.png" alt="Plus"/>
          <img src="kampagne/img/minus.png" alt="Minus"/>
		  <span class="language">DE | FR</span>
	    </div>

	  <div class="navigation primary">
	    <ul style="display: block;" id="menu" role="navigation">
	      <li class="link"><a href="#sehbehinderung" title="Sehbehinderung">Sehbehinderung</a></li>			  
	      <li class="link"><a href="#sbvhilft" title="Der SBV hilft">Der SBV hilft</a></li>
	      <li class="link"><a href="#kampagne" title="Kampagne">Kampagne</a></li>		  
	      <li class="link"><a href="#medien" title="Medien">Medien</a></li>
	      <li class="link"><a href="http://www.sbv-fsa.ch/" title="Hauptseite SBV">Hauptseite SBV</a></li>		
	  	  <div class="wrapper sbvclear spacer"></div>
	    </ul>
	  </div>



	</div>
  </div>



	  <!-- 
	       ***********************
	       Inhalt
	       ***********************
	    -->
  <div class="main" role="main">
	<div class="content-container typography">	



	  
	  <div class="wrapper sbvblue">
	    <div id="abca" class="sbvblue  article">
	      <div class="text middle">
		<h1 class="middle start">Eine Sehbehinderung macht den Alltag zur Herausforderung.</h1>
		<br /><br /><br />
		<p><a href="#kampagne" aria-hidden="true"><img aria-hidden="true" src="kampagne/img/arrow_down_white.png"/></a></p>
	      </div>

	    </div>
	  </div>
	  <div style="clear:both"></div>



	  <div class="wrapper sbvclear">		
	    <div id="kampagne" class="sbvclear article">
	      <div class="text">
		<h1>Kampagnen Information</h1>
		<p>Noch zuviele Barrieren schränken blinde und sehbehinderte Menschen in ihrem Alltag ein. Ganz einfach deshalb, weil sehende Menschen sich dessen nicht bewusst sind und oftmals in Unkenntnis vorhandener Lösungen für betroffene Menschen.
		</p>
		<p>Der Schweizerische Blinden- und Sehbehindertenverband  will diese Situation mit einer Sensibilisierungskampagne entschärfen. Die Kampagne richtet sich sowohl an die breite Bevölkerung als auch an betroffene Menschen und ihre Nächsten.
		</p>
		<p>
		  <a>Zur Sensibilisierungskampagne</a>
		</p>
	      </div>
	    </div>
	  </div>
	  <div style="clear:both"></div>
	  
	  
	  

	  <div class="wrapper sbvblue">
	    <div id="sehbehinderung" class="sbvblue Alltag article">
	      <div class="text">
		    <h1>Sehbehinderung im Alltag</h1>

		<ul class="AlltagSlider">
		  <li>
		    <p>Fehlt in der Bedienung des Aufzugs eine ertastbare oder akustische Anzeige, fehlt für blinde und sehbehinderte Menschen die Orientierung fürs richtige Stockwerk.</p>
		    <img src="kampagne/img/Sehbehinderung0.jpg"/ >
		  </li>

		  <li>
		    <p>Moderne Billettautomaten mit Touchscreen haben den persönlich betreuten Billettschalter ersetzt. Liest ein Touchscreen nicht vor, was geschieht, ist er für blinde und sehbehinderte Menschen nicht zu bedienen.</p>
		    <img src="kampagne/img/Sehbehinderung1.jpg"/ >
		  </li>

		  <li>
		    <p>Selbst wenn Smartphones das Leben blinder und sehbehinderter Menschen erleichtern sind für jede Neuerung entsprechende Anpassungen erforderlich, beispielsweise mittels Spracherkennung.</p>
		    <img src="kampagne/img/Sehbehinderung2.jpg"/ >
		  </li>

		  <li>
		    <p>Wie alles Schriftliche bleibt auch die Speisekarte im Restaurant für den blinden und sehbehinderten Menschen stumm. Erst das Vorlesen erschliesst das Speiseangebot.</p>
		    <img src="kampagne/img/Sehbehinderung3.jpg"/ >
		  </li>

		  <li>
		    <p>Das herannahende Tram trägt die Liniennummer gut sichtbar auf der Frontseite. Menschen mit einer Sehbehinderung können visuelle Informationen oft nicht entziffern, sie müssen sich mittels anderer Informationskanäle ein Bild machen.</p>
		    <img src="kampagne/img/Sehbehinderung4.jpg"/ >
		  </li>

		  <li>
		    <p>Moderne Kaffeemaschinen produzieren fortzu raffiniertere und exklusivere Getränke. Die Bedienung via Sensortaste und damit die Wahl des gewünschten Getränks ist für einen Menschen mit Sehbehinderung praktisch unmöglich.</p>
		    <img src="kampagne/img/Sehbehinderung5.jpg"/ >
		  </li>
		</ul>

	      </div>
	    </div>
	  </div>
	  <div style="clear:both"></div>





	  
	  <div class="wrapper sbvclear">
	    <div id="sbvhilft" class="sbvclear article">
	      <div class="text">
		<h1>Wie der SBV hilft</h1>
		<p>Mit speziellem Dienstleistungsangebot, professioneller Beratung und Unterstützung ebenso wie mit engagierter Interessenvertretung leistet der SBV einen entscheidenden Beitrag an die Integration blinder und sehbehinderter Menschen ins politische, wirtschaftliche, soziale und kulturelle Leben. 
		  <br />16 Sektionen in der ganzen Schweiz praktizieren die private Selbsthilfe von betroffenen für betroffene Menschen. Beratungsstellen 	in Basel, Biel, Chur, Delsberg, Freiburg, Lausanne, Luzern, Sitten, Tenero und Zürich helfen, mit einer Sehbehinderung umzugehen. Erwachsenenbildungsangebote fördern die individuellen Stärken und vermitteln Fertigkeiten namentlich auch im Umgang mit moderner Informationstechnologie. Bildungs- und Begegnungszentren in Bern, Dietikon/Zürich, Horw/Luzern, Lausanne und Sankt Gallen ebenso wie Kreativgruppen in der ganzen Schweiz bieten alles für Handwerk und Kreation mit Sehbehinderung. Freizeitangebote machen Abwechslung und gemeinsames Erleben unter Betroffenen möglich.
		</p>
		<p>
		  <a href="http://www.sbv-fsa.ch/de/beratungsstellen" target="_blank">Zu den Beratungsstellen</a>
		</p>
	      </div>

	    </div>
	  </div>
	  <div style="clear:both"></div>
		
	  <div class="wrapper sbvblue">
	    <div id="sbvhilft" class="sbvblue  article">
	      <div class="text">
		<ul class="PlakatSlider">
		  <li>
		    <div class="middle"><img src="kampagne/img/D/065021402_Plakate_D_F200_ML_web.jpg"/ ></div>
	          </li>
		  <li>
		    <div class="middle"><img src="kampagne/img/D/065021402_Plakate_D_F200_ML2_web.jpg"/ ></div>
	          </li>
		  <li>
		    <div class="middle"><img src="kampagne/img/D/065021402_Plakate_D_F200_ML3_web.jpg"/ ></div>
		  </li>
		</ul>
		

		<p>
		<br /><br /><br />Mit der Darstellung der Konsequenzen einer Sehbehinderung zielt die Sensibilisierungskampagne die Aufmerksamkeit auf sich: Nicht betroffene Menschen sehen sich in die Situation einer Person mit Sehbehinderung versetzt. Gleichzeitig erkennen sich Betroffene in ihrer Alltagsherausforderung wieder.
		</p><p>Die Sensibilisierungskampagne inszeniert Alltagsgegenstände in ihrer Verwechslungssituation mit sehr ähnlichen Objekten. Die Kampagne spielt mit dieser Verwechslung, sie löst bereits bei sehenden Menschen spontane Irritation aus. Wer sich der Herausforderungen einer Sehbehinderung im Alltag bewusst ist, erkennt die zahlreichen Barrieren und Hindernisse für sehbehinderte Menschen. 
		</p><p>Wer als blinder oder sehbehinderter Mensch nicht von spezieller Betreuung und Hilfsmitteln profitiert, vermag sich schwerlich in die Gesellschaft zu integrieren. Im Wissen um das eigene Handicap (beispielsweise jener Menschen, welche davon ausgehen, bloss eine Sehschwierigkeit zu haben) und in Kenntnis und Inanspruchnahme aller möglichen Hilfe und Unterstützung wird es betroffenen Menschen gelingen, die eigene Selbstständigkeit zu erhöhen und damit die Teilhabe an der Gesellschaft zu erlangen.
		</p>



	      </div>

	    </div>
	  </div>
	  <div style="clear:both"></div>
	  
	  
	  

	  <div class="wrapper sbvclear">		
	    <div  id="medien" class="sbvclear  article">
	      <div class="text">
		<h1>Medienmitteilung / Medienmappe</h1>
		<p>Der Schweizerische Blinden- und Sehbehindertenverband SBV sensibilisiert die Bevölkerung mit einer neuen Kampagne für das Thema Sehbehinderung. Auf Plakaten und in TV-Spots werden Betrachtende mit Konsumgütern konfrontiert, deren richtiges Erkennen für Sehbehinderte die alltägliche Herausforderung ist. Der SBV als von Betroffenen geführte nationale Organisation hilft.
		</p>

		<p>
		  <div class="halfbox">
		    <h2>Downloads</h2>
		    <a>TV-Spot MPEG</a><br/>
		    <a>Kampagnen Sujets PDF</a><br/>
		    <a href="data/Medienmitteilung.pdf">Pressemitteilung PDF</a><br/>
		  </div>
		  <div class="halfbox">
		    <h2>Kontakt</h2>
		    <p>Kannarath Meystre, <br />Generalsekretär SBV, <br />Tel. 079 212 35 37</p>
		    <p>Alfred Rikli, <br />Leiter Interessenvertretung SBV, <br />Tel. 077 457 50 79</p>
		  </div>
		</p>
		<p>
		  <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
		  Für Werbematerialbestellung wenden Sie sich bitte an <br /><a href="mailt:marketing@sbv-fsa.ch">marketing@sbv-fsa.ch</a>
		</p>
	      </div>
	    </div>
	  </div>
	  <div style="clear:both"></div>




	  <!-- 
	       ***********************
	       Ende Inhalt
	       ***********************
	    -->
	</div>
      </div>


      <footer class="footer" role="contentinfo">
	<div class="inner copy">
	  <div class="left"><!--SBV <span class="arrow">→</span> -->
	    <nav class="primary">
	      <ul style="display: block;">
		<li class="link"><a href="#sehbehinderung" title="Sehbehinderung">Sehbehinderung</a></li>			  
		<li class="link"><a href="#sbvhilft" title="Der SBV hilft">Der SBV hilft</a></li>
		<li class="link"><a href="#kampagne" title="Kampagne">Kampagne</a></li>		  
		<li class="link"><a href="#medien" title="Medien">Medien</a></li>
		<li class="link"><a href="#" title="Hauptseite SBV">Hauptseite SBV</a></li>		  
	      </ul>
	    </nav>
	  </div>
	</div>
      </footer>


      <script type="text/javascript" src="kampagne/js/jquery-1.js"></script>
      <script type="text/javascript" src="kampagne/js/script.js"></script></div>

<!-- bxSlider -->
<script src="kampagne/js/jquery.bxslider.monkee.accessible.js"></script>
<link href="kampagne/css/jquery.bxslider.css" rel="stylesheet" />

<script type="text/javascript">
    

  jQuery(document).ready(function(){
    jQuery('.AlltagSlider').bxSlider({
      pager: false,
      auto: false,
      pause: 6000,
      preventDefaultSwipeY: false,
      preventDefaultSwipeX: false,
      responsive: true,
      slideMargin: 10,
	  infiniteLoop:false,
	  hideControlOnEnd:true
    });

    jQuery('.PlakatSlider').bxSlider({
      pager: false,
      auto: false,
      pause: 6000,
      preventDefaultSwipeY: false,
      responisve: true,
      slideMargin: 10,
	  infiniteLoop:false,
	  hideControlOnEnd:true
    });

	var menu = jQuery('.tablet-nav .header .primary ul');
        menu.addClass("ScreenReader");
    
  });



  jQuery(window).resize(function() {

    jQuery('.AlltagSlider').bxSlider().reloadSlider();
    jQuery('.PlakatSlider').bxSlider().reloadSlider();


  });


</script>




<!--  SBV SBV SBV -->
<!--  SBV SBV SBV -->
<!--  SBV SBV SBV -->










<h1>OLD OLD OLD</h1>
<div id="page-wrapper"><div id="page">

  <div id="header" class="homepage"><div class="section clearfix">

    <?php if ($logo): ?>
      <!--<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">-->
		<a href="<?php echo sbvmainurl(); ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <!--<img src="/sites/all/themes/sbvfsa/img/logo_deutsch.jpg" alt="<?php print t('Home'); ?>" /> -->
		<span class="logo <?php print $language->language; ?>"></span>
      </a>
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
      <div id="name-and-slogan"<?php if ($hide_site_name && $hide_site_slogan) { print ' class="element-invisible"'; } ?>>

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
              <strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>>
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>

      </div> <!-- /#name-and-slogan -->
    <?php endif; ?>

    <?php print render($page['header']); ?>


    <?php if (false && $main_menu): // Disabled ?>
      <div id="main-menu" class="navigation">
        <?php print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'main-menu-links',
            'class' => array('links', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Main menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </div> <!-- /#main-menu -->
    <?php endif; ?>

    <?php if (false && $secondary_menu): ?>
      <div id="secondary-menu" class="navigation">
        <?php print theme('links__system_secondary_menu', array(
          'links' => $secondary_menu,
          'attributes' => array(
            'id' => 'secondary-menu-links',
            'class' => array('links', 'inline', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Secondary menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </div> <!-- /#secondary-menu -->
    <?php endif; ?>

  </div></div> <!-- /.section, /#header -->

  <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>

  <?php if ($page['featured']): ?>
    <div id="featured"><div class="section clearfix">
      <?php print render($page['featured']); ?>
    </div></div> <!-- /.section, /#featured -->
  <?php endif; ?>

  <div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">

    <?php if (false && $breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
    <?php endif; ?>

    <div id="content" class="column"><div class="section">
      <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
      <a id="main-content"></a>
      <?php if ($tabs): ?>
        <div class="tabs">
          <?php print render($tabs); ?>
        </div>
      <?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>

    </div></div> <!-- /.section, /#content -->


  </div></div> <!-- /#main, /#main-wrapper -->

<!-- Monkee Image Changer  -->
	<script type="text/javascript">

		var tempo = '';


		jQuery('li.box.sehbehinderung').hover(function(){
			jQuery('li img').hide(tempo);
			jQuery('li.mithelfen img').show();
			jQuery('li img.sehbehinderung').show(tempo);
		});
		jQuery('li.box.dienste').hover(function(){
			jQuery('li img').hide(tempo);
			jQuery('li.mithelfen img').show();
			jQuery('li img.dienste').show(tempo);
		});
		jQuery('li.box.verband').hover(function(){
			jQuery('li img').hide(tempo);
			jQuery('li.mithelfen img').show();
			jQuery('li img.verband').show(tempo);
		});
		jQuery('li.box.aktuell').hover(function(){
			jQuery('li img').hide(tempo);
			jQuery('li.mithelfen img').show();
			jQuery('li img.aktuell').show(tempo);
		});
		jQuery('li.box.brett').hover(function(){
			jQuery('li img').hide(tempo);
			jQuery('li.mithelfen img').show();
			jQuery('li img.brett').show(tempo);
		});
		jQuery('li.box.links').hover(function(){
			jQuery('li img').hide(tempo);
			jQuery('li.mithelfen img').show();
			jQuery('li img.links').show(tempo);
		});
		jQuery('li.box.medien').hover(function(){
			jQuery('li img').hide(tempo);
			jQuery('li.mithelfen img').show();
			jQuery('li img.medien').show(tempo);
		});
		jQuery('li.box.mithelfen').hover(function(){
			jQuery('li img').hide(tempo);
			jQuery('li.mithelfen img').show();
			jQuery('li img.mithelfen').show(tempo);
		});


/*Link expander ;-) */
               	jQuery('li.box p').click(function(){
 
		    location.href = "http://www.sbv-fsa.ch/"+jQuery(this).parent().find('h2 a').attr('href');
		});
    

	</script>

  <?php if ($page['triptych_first'] || $page['triptych_middle'] || $page['triptych_last']): ?>
    <div id="triptych-wrapper"><div id="triptych" class="clearfix">
      <?php print render($page['triptych_first']); ?>
      <?php print render($page['triptych_middle']); ?>
      <?php print render($page['triptych_last']); ?>
    </div></div> <!-- /#triptych, /#triptych-wrapper -->
  <?php endif; ?>

  <div id="footer-wrapper"><div class="section">

    <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
      <div id="footer-columns" class="clearfix">
        <?php print render($page['footer_firstcolumn']); ?>
        <?php print render($page['footer_secondcolumn']); ?>
        <?php print render($page['footer_thirdcolumn']); ?>
        <?php print render($page['footer_fourthcolumn']); ?>
      </div> <!-- /#footer-columns -->
    <?php endif; ?>

    <?php if ($page['footer']): ?>
      <div id="footer" class="clearfix">
        <?php print render($page['footer']); ?>
      </div> <!-- /#footer -->
    <?php endif; ?>

  </div></div> <!-- /.section, /#footer-wrapper -->

</div></div> <!-- /#page, /#page-wrapper -->


</html>
