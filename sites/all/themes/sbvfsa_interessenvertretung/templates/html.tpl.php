<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<?php global $language ?>
<head profile="<?php print $grddl_profile; ?>">	
  <?php print $head; ?>
  <title>
    <?php 
    if($language->language == 'fr'){
      print $head_title_array['title'] .' | FSA';
    } else {
      print $head_title;
    } 
    ?>
  </title>
  <!-- <META HTTP-EQUIV="Content-Language" CONTENT="<?php print $language->language; ?>"> -->
  <?php print $styles; ?>
  <link rel="alternate stylesheet" title="High Contrast" href="<?php print base_path() . path_to_theme();?>/css/negativ.css" media="screen"></link>
  <link rel="alternate stylesheet" title="Left" href="<?php print base_path() . path_to_theme();?>/css/left.css" media="screen"></link>
  <link rel="alternate stylesheet" title="Contrast Left" href="<?php print base_path() . path_to_theme();?>/css/negativLeft.css" media="screen"></link>



  <!-- Youtube Accessibility -->
  <link href="<?php print base_path() . path_to_theme();?>/thirdparty/ytp/css/ytp.css" type="text/css" rel="stylesheet"></link>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>

  <?php print $scripts; ?>

  <!-- Youtube Accessibility -->
  <script src="<?php print base_path() . path_to_theme();?>/thirdparty/ytp/js/ytp.js" type="text/javascript"></script>

  <script src="<?php print base_path() . path_to_theme();?>/js/styleswitcher.js" type="text/javascript"></script>
  <script src="<?php print base_path() . path_to_theme();?>/js/monkee.js" type="text/javascript"></script>

  <?php
 	if($language->language == 'fr') {
       	    echo '<script src="http://f1.eu.readspeaker.com/script/6122/rs_embhl_v2_de_de.js" type="text/javascript"></script>';   
	    echo '<script src="http://f1.eu.readspeaker.com/script/6122/rs_embhl_v2_fr_fr.js" type="text/javascript"></script>';
	} else {
            echo '<script src="http://f1.eu.readspeaker.com/script/6122/rs_embhl_v2_fr_fr.js" type="text/javascript"></script>';
	    echo '<script src="http://f1.eu.readspeaker.com/script/6122/rs_embhl_v2_de_de.js" type="text/javascript"></script>';	
	} 
  ?>


</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>


 <?php

	  echo '<div id="skip-link">
		    <a href="#haupt-menu" class="element-invisible element-focusable" accesskey="1">'. t('Menü') .'</a>
	 	    <a href="#page-title" class="element-invisible element-focusable" accesskey="2">'. t('content') .'</a>
                   <!-- <a href="#edit-search-block-form--2" class="element-invisible element-focusable" accesskey="5">'. t('Suche') .'</a>
                    <a href="#accesstools" class="element-invisible element-focusable" accesskey="6">'. t('Darstellung') .'</a> -->
		</div>';

    ?>	


<!-- WebForms Accessibility Fix -->
<script type="text/javascript">
    jQuery('input.required').attr('aria-required',true);
</script>


</body>
</html>
