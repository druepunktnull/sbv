(function($) {

/**
 * jQuery debugging helper.
 *
 * Invented for Dreditor.
 *
 * @usage
 *   $.debug(var [, name]);
 *   $variable.debug( [name] );
 */
jQuery.extend({
  debug: function () {
    // Setup debug storage in global window. We want to look into it.
    window.debug = window.debug || [];

    args = jQuery.makeArray(arguments);
    // Determine data source; this is an object for $variable.debug().
    // Also determine the identifier to store data with.
    if (typeof this == 'object') {
      var name = (args.length ? args[0] : window.debug.length);
      var data = this;
    }
    else {
      var name = (args.length > 1 ? args.pop() : window.debug.length);
      var data = args[0];
    }
    // Store data.
    window.debug[name] = data;
    // Dump data into Firebug console.
    if (typeof console != 'undefined') {
      console.log(name, data);
    }
    return this;
  }
});
// @todo Is this the right way?
jQuery.fn.debug = jQuery.debug;

})(jQuery);
;
(function ($) {

Drupal.behaviors.filetree = {

  attach: function(context, settings) {

    // Collapse the sub-folders.
    $('.filetree .files ul').hide();

    // Expand/collapse sub-folder when clicking parent folder.
    $('.filetree .files li.folder').click(function(e) {
      // A link was clicked, so don't mess with the folders.
      if ($(e.target).is('a')) {
        return;
      }
      // Determine whether or not to use an animation when toggling folders.
      var animation = $(this).parents('.filetree').hasClass('filetree-animation') ? 'fast' : '';
      // If multiple folders are not allowed, collapse non-parent folders.
      if (!$(this).parents('.filetree').hasClass('multi')) {
        $(this).parents('.files').find('li.folder').not($(this).parents()).not($(this)).removeClass('expanded').find('ul:first').hide(animation);
      }
      // Expand.
      if (!$(this).hasClass('expanded')) {
        $(this).addClass('expanded').find('ul:first').show(animation);
      }
      // Collapse.
      else {
        $(this).removeClass('expanded').find('ul:first').hide(animation);
      }
      // Prevent collapsing parent folders.
      return false;
    });

    // Expand/collapse all when clicking controls.
    $('.filetree .controls a').click(function() {
      var animation = $(this).parents('.filetree').hasClass('filetree-animation') ? 'fast' : '';
      if ($(this).hasClass('expand')) {
        $(this).parents('.filetree').find('.files li.folder').addClass('expanded').find('ul:first').show(animation);
      }
      else {
        $(this).parents('.filetree').find('.files li.folder').removeClass('expanded').find('ul:first').hide(animation);
      }
      return false;
    });

  }

};

})(jQuery);
;
(function ($) {

  /**
   * A placeholder to save the currently focused input between AJAX requests.
   */
  Drupal.projectIssueFocusedInput = false;

  /**
   * Persistent input focus (retains the proper focus after AJAX replacements).
   */
  Drupal.behaviors.projectIssueFocusedInput = {
    attach: function (context) {
      var $context = $(context);
      var inputs = ':input:not([type="hidden"],:submit)';
      // Only bind a top level click event once to remove the stored focus if
      // element is not an input.
      $('body').once('project-issue-focused-input', function () {
        $(this).bind('mousedown', function (e) {
          if (!$(e.target).is(inputs)) {
            Drupal.projectIssueFocusedInput = false;
          }
        });
      });
      if (Drupal.projectIssueFocusedInput) {
        // Do not use $context here, focused input could not be part of it.
        $(':input[name="' + Drupal.projectIssueFocusedInput + '"]').focus();
      }
      // Bind various events on input elements to ensure we save the
      // proper currently focused element.
      var $inputs = $context.find(inputs)
        .bind({
          'keydown': function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            // Detect tab keystroke.
            if (code === 9) {
              // Obtain the name of the next input element in the DOM
              // structure.
              Drupal.projectIssueFocusedInput = $inputs.eq(parseInt($inputs.index($(this)), 10) + 1).attr('name');
            }
          },
          'mousedown': function () {
            Drupal.projectIssueFocusedInput = $(this).attr('name');
          },
          'focus': function () {
            Drupal.projectIssueFocusedInput = $(this).attr('name');
          }
        });
    }
  };

  /**
   * Persistent issue fieldsets (keeps them open/closed between HTTP requests).
   */
  Drupal.behaviors.projectIssuePersistentFieldsets = {
    attach: function () {
      // Only continue if localStorage is supported in the browser.
      if ('localStorage' in window && typeof window.localStorage !== 'undefined' && window['localStorage'] !== null) {
        // Only bind once.
        $('#project-issue-ajax-form').once('issue-form', function () {
          var prefix = $(this).parents('[id]').attr('id');
          $('fieldset.collapsible', this).each(function() {
            var $fieldset = $(this);
            var id = prefix + '.' + $fieldset.attr('id');
            // If the ID of the fieldset is present in localStorage, change open status.
            if (typeof window.localStorage[id] !== 'undefined') {
              if (window.localStorage[id] == 'true') { // localStorage casts to string.
                $fieldset.removeClass('collapsed');
              }
              else {
                $fieldset.addClass('collapsed');
              }
            }
            // Bind the "collapsed" event to change the localStorage value.
            $fieldset.bind('collapsed', function(e) {
              window.localStorage[id] = !e.value;
            });
          });
        });
      }
    }
  };

  /**
   * Jump to the actual preview and the form.
   */
  Drupal.behaviors.projectIssueJumpToNodeEditBlock = {
    attach: function () {
      $('div.preview, table.diff').once(function() {
        $('html, body').animate({scrollTop: $('#project-issue-ajax-form').offset().top});
      });
    }
  };

})(jQuery);
;
